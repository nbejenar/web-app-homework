package com.endava.internship.swaggerdemo.repository.jpa;

import com.endava.internship.swaggerdemo.model.Employee;
import com.endava.internship.swaggerdemo.repository.EmployeeRepository;
import org.springframework.context.annotation.Profile;
import org.springframework.data.jpa.repository.JpaRepository;

@Profile("jpa")
public interface EmployeeJpaRepository extends JpaRepository<Employee, Long>, EmployeeRepository {

}
