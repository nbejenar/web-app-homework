package com.endava.internship.swaggerdemo.repository;

import com.endava.internship.swaggerdemo.model.JobHistory;
import com.endava.internship.swaggerdemo.model.JobHistoryId;
import org.springframework.data.jpa.repository.JpaRepository;

public interface JobHistoryRepository extends JpaRepository<JobHistory, JobHistoryId> {
}
