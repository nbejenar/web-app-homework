package com.endava.internship.swaggerdemo.repository.jdbc;

import com.endava.internship.swaggerdemo.model.Department;
import com.endava.internship.swaggerdemo.model.Employee;
import com.endava.internship.swaggerdemo.model.Job;
import com.endava.internship.swaggerdemo.repository.EmployeeRepository;
import org.springframework.context.annotation.Profile;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Repository
@Profile("jdbc")
public class EmployeeJdbcRepository implements EmployeeRepository {

    private final JdbcTemplate jdbcTemplate;

    public EmployeeJdbcRepository(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    @Transactional(readOnly = true)
    public List<Employee> findAll() {
        String sqlQuery = "select * from employees";

        return jdbcTemplate.query(sqlQuery, this::mapRowToEmployee);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<Employee> findById(Long id) {
        checkNull(id);

        String sqlQuery = "select * " +
                "from employees where EMPLOYEE_ID = ?";

        List<Employee> list = jdbcTemplate.query(sqlQuery, this::mapRowToEmployee, id);
        if (list.isEmpty()) return Optional.empty();

        return Optional.ofNullable(list.get(0));
    }

    @Override
    @Transactional
    public Employee save(Employee employee) {
        checkNull(employee);

        if (employee.getId() != null && findById(employee.getId()).isPresent()) {
            return update(employee);
        }

        SimpleJdbcInsert simpleJdbcInsert = new SimpleJdbcInsert(jdbcTemplate)
                .withTableName("employees")
                .usingGeneratedKeyColumns("employee_id");

        long id = simpleJdbcInsert.executeAndReturnKey(employee.toMap()).longValue();
        return findById(id).get();
    }

    @Override
    @Transactional
    public void deleteById(Long id) {
        checkNull(id);

        String sqlQuery = "delete from employees where EMPLOYEE_ID = ?";

        jdbcTemplate.update(sqlQuery, id);
    }

    private Employee update(Employee employee) {
        String sqlQuery = "update employees set " +
                "first_name=?, last_name=?, EMAIL=?, PHONE_NUMBER=?, HIRE_DATE=?, JOB_ID=?, SALARY=?, " +
                "COMMISSION_PCT=?, MANAGER_ID=?, DEPARTMENT_ID=?" +
                "where EMPLOYEE_ID = ?";


        Object departmentId = employee.getDepartment() == null ? null : employee.getDepartment().getId();
        Object managerId = employee.getManager() == null ? null : employee.getManager().getId();
        Object jobId = employee.getJob() == null ? null : employee.getJob().getId();
        jdbcTemplate.update(sqlQuery
                , employee.getFirstName()
                , employee.getLastName()
                , employee.getEmail()
                , employee.getPhoneNumber()
                , employee.getHireDate()
                , jobId
                , employee.getSalary()
                , employee.getCommissionPct()
                , managerId
                , departmentId
                , employee.getId());

        return findById(employee.getId()).get();
    }

    private void checkNull(Object obj) {
        if (obj == null) throw new IllegalArgumentException();
    }

    private Employee mapRowToEmployee(ResultSet resultSet, int rowNum) throws SQLException {
        Employee.EmployeeBuilder employeeBuilder = Employee.builder()
                .id(resultSet.getLong("employee_id"))
                .firstName(resultSet.getString("first_name"))
                .lastName(resultSet.getString("last_name"))
                .salary(resultSet.getBigDecimal("salary"))
                .phoneNumber(resultSet.getString("phone_number"))
                .hireDate(resultSet.getDate("hire_date").toLocalDate())
                .email(resultSet.getString("email"))
                .commissionPct(resultSet.getBigDecimal("commission_pct"));

        Long dId = resultSet.getLong("department_id");
        Long mId = resultSet.getLong("manager_id");
        String jId = resultSet.getString("job_id");

        if (dId != 0)
            employeeBuilder = employeeBuilder.department(Department.builder().id(dId).build());
        if (mId != 0)
            employeeBuilder = employeeBuilder.manager(Employee.builder().id(mId).build());
        if (jId != null)
            employeeBuilder = employeeBuilder.job(Job.builder().id(jId).build());

        return employeeBuilder.build();
    }
}
