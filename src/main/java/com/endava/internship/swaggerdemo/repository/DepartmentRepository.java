package com.endava.internship.swaggerdemo.repository;

import com.endava.internship.swaggerdemo.model.Department;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DepartmentRepository extends JpaRepository<Department, Long> {
}
