package com.endava.internship.swaggerdemo.repository;

import com.endava.internship.swaggerdemo.model.Job;
import org.springframework.data.jpa.repository.JpaRepository;

public interface JobRepository extends JpaRepository<Job, String> {
}
