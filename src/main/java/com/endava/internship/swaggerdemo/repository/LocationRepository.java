package com.endava.internship.swaggerdemo.repository;

import com.endava.internship.swaggerdemo.model.Location;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LocationRepository extends JpaRepository<Location, Integer> {
}
