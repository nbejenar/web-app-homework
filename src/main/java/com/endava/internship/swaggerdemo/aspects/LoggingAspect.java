package com.endava.internship.swaggerdemo.aspects;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.stereotype.Component;

import java.time.Duration;

@Aspect
@Component
@Slf4j
@ConditionalOnExpression("${app.logging.logging-ascpect.enabled:true}")
public class LoggingAspect {

    @Around("target(com.endava.internship.swaggerdemo.repository.EmployeeRepository)")
    public Object logPerformance(ProceedingJoinPoint proceedingJoinPoint) throws Throwable {
        long startTime = System.currentTimeMillis();
        try {
            return proceedingJoinPoint.proceed();
        } finally {
            long finishTime = System.currentTimeMillis();
            Duration duration = Duration.ofMillis(finishTime - startTime);

            log.info("Duration of {} execution was {}", proceedingJoinPoint.getSignature(), duration);
        }
    }

    @Around("execution(* com.endava.internship.swaggerdemo.service.*ServiceImpl.*(..))")
    public Object logServiceLayer(ProceedingJoinPoint proceedingJoinPoint) throws Throwable {
        try {
            Object o = proceedingJoinPoint.proceed();
            log.debug("Successfully executed {} with args {}",
                    proceedingJoinPoint.getSignature(), proceedingJoinPoint.getArgs());
            return o;
        } catch (Throwable t) {
            log.debug("findById of {} with args {}  resulted in exception: {}",
                    proceedingJoinPoint.getSignature(), proceedingJoinPoint.getArgs(), t.getMessage());
            throw t;
        }
    }

}
