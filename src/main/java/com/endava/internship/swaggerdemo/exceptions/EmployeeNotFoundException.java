package com.endava.internship.swaggerdemo.exceptions;

import lombok.Getter;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class EmployeeNotFoundException extends RuntimeException {

    @Getter
    private final Long id;

    public EmployeeNotFoundException(Long id) {
        super("Employee with id " + id + " was not found");
        this.id = id;
    }
}
