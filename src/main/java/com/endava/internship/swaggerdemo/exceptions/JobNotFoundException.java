package com.endava.internship.swaggerdemo.exceptions;

import lombok.Getter;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class JobNotFoundException extends RuntimeException {

    @Getter
    private final String id;

    public JobNotFoundException(String id) {
        super("Job with id " + id + " was not found");
        this.id = id;
    }
}
