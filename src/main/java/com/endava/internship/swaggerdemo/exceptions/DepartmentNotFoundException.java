package com.endava.internship.swaggerdemo.exceptions;

import lombok.Getter;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class DepartmentNotFoundException extends RuntimeException {

    @Getter
    private final Long id;

    public DepartmentNotFoundException(Long id) {
        super("Department with id " + id + " was not found");
        this.id = id;
    }
}