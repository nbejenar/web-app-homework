package com.endava.internship.swaggerdemo.exceptions;

import lombok.Getter;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class LocationNotFoundException extends RuntimeException {

    @Getter
    private final Integer id;

    public LocationNotFoundException(Integer id) {
        super("Location with id " + id + " was not found");
        this.id = id;
    }
}
