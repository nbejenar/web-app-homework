package com.endava.internship.swaggerdemo.exceptions;

import com.endava.internship.swaggerdemo.controller.DepartmentRestController;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.util.Locale;

import static com.endava.internship.swaggerdemo.exceptions.ErrorMessage.LOCATION_NOT_FOUND_EXCEPTION;

@ControllerAdvice(assignableTypes = DepartmentRestController.class)
public class DepartmentExceptionHandler {

    private final MessageSource properties;

    public DepartmentExceptionHandler(@Qualifier("messageSource") MessageSource properties) {
        this.properties = properties;
    }

    @ExceptionHandler(LocationNotFoundException.class)
    public ResponseEntity<ErrorResponse> handleLocationNotFoundException(LocationNotFoundException e) {
        return ResponseEntity
                .status(HttpStatus.NOT_FOUND)
                .body(new ErrorResponse(HttpStatus.NOT_FOUND.value(),
                        getMessage(LOCATION_NOT_FOUND_EXCEPTION, e.getId())));
    }

    private String getMessage(String message, Object... objects) {
        return properties.getMessage(message, objects, getLocale());
    }

    private Locale getLocale() {
        return LocaleContextHolder.getLocale();
    }

}
