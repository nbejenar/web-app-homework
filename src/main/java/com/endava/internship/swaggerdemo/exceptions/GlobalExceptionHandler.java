package com.endava.internship.swaggerdemo.exceptions;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import static com.endava.internship.swaggerdemo.exceptions.ErrorMessage.DEPARTMENT_NOT_FOUND_EXCEPTION;
import static com.endava.internship.swaggerdemo.exceptions.ErrorMessage.EMPLOYEE_NOT_FOUND_EXCEPTION;
import static com.endava.internship.swaggerdemo.exceptions.ErrorMessage.NUMBER_FORMAT_EXCEPTION;


@ControllerAdvice
public class GlobalExceptionHandler {

    private final MessageSource properties;

    public GlobalExceptionHandler(@Qualifier("messageSource") MessageSource properties) {
        this.properties = properties;
    }

    //this exception can be thrown in both controllers, so there is no need to extract it
    @ExceptionHandler(EmployeeNotFoundException.class)
    public ResponseEntity<ErrorResponse> handleEmployeeNotFound(EmployeeNotFoundException e) {
        return ResponseEntity
                .status(HttpStatus.NOT_FOUND)
                .body(new ErrorResponse(HttpStatus.NOT_FOUND.value(),
                        getMessage(EMPLOYEE_NOT_FOUND_EXCEPTION, e.getId())));
    }

    //this exception can be thrown in both controllers, so there is no need to extract it
    @ExceptionHandler(DepartmentNotFoundException.class)
    public ResponseEntity<ErrorResponse> handleDepartmentNotFoundException(DepartmentNotFoundException e) {
        return ResponseEntity
                .status(HttpStatus.NOT_FOUND)
                .body(new ErrorResponse(HttpStatus.NOT_FOUND.value(),
                        getMessage(DEPARTMENT_NOT_FOUND_EXCEPTION, e.getId())));
    }

    @ExceptionHandler(NumberFormatException.class)
    public ResponseEntity<ErrorResponse> handleNumberFormatException(Exception e) {
        return ResponseEntity
                .status(HttpStatus.BAD_REQUEST)
                .body(new ErrorResponse(HttpStatus.BAD_REQUEST.value(),
                        getMessage(NUMBER_FORMAT_EXCEPTION)));
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity handleValidationExceptions(MethodArgumentNotValidException ex) {
        Map<String, String> errors = new HashMap<>();
        ex.getBindingResult().getAllErrors().forEach((error) -> {
            String fieldName = ((FieldError) error).getField();
            String errorMessage = error.getDefaultMessage();
            errors.put(fieldName, errorMessage);
        });
        return ResponseEntity
                .status(HttpStatus.BAD_REQUEST)
                .body(errors);
    }


    private String getMessage(String message, Object... objects) {
        return properties.getMessage(message, objects, getLocale());
    }

    private Locale getLocale() {
        return LocaleContextHolder.getLocale();
    }
}
