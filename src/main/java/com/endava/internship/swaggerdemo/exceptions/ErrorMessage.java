package com.endava.internship.swaggerdemo.exceptions;

public interface ErrorMessage {

    String EMPLOYEE_NOT_FOUND_EXCEPTION = "employee.exception.employee-not-found-exception";
    String DEPARTMENT_NOT_FOUND_EXCEPTION = "employee.exception.department-not-found-exception";
    String LOCATION_NOT_FOUND_EXCEPTION = "employee.exception.location-not-found-exception";
    String NUMBER_FORMAT_EXCEPTION = "employee.exception.number-format-exception";
    String RESOURCE_NOT_FOUND_EXCEPTION = "resource.exception.resource-not-found";
}
