package com.endava.internship.swaggerdemo.controller;

import com.endava.internship.swaggerdemo.facade.DepartmentFacade;
import com.endava.internship.swaggerdemo.model.dto.DepartmentDTO;
import com.endava.internship.swaggerdemo.model.dto.Identifiable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/v1.0/department")
public class DepartmentRestController {

    private final DepartmentFacade departmentFacade;

    public DepartmentRestController(DepartmentFacade departmentFacade) {
        this.departmentFacade = departmentFacade;
    }

    @GetMapping
    public ResponseEntity<List<Identifiable<Long>>> getAlldepartments(@RequestParam(required = false) boolean expand) {
        return ResponseEntity.ok(departmentFacade.findAll(expand));
    }

    @GetMapping("/{id}")
    public ResponseEntity<Identifiable<Long>> getdepartment(@PathVariable Long id,
                                                            @RequestParam(required = false) boolean expand) {
        Identifiable<Long> department = departmentFacade.findById(id, expand);
        return ResponseEntity.ok(department);
    }

    @PostMapping
    public DepartmentDTO addDepartment(@RequestBody @Valid DepartmentDTO department) {
        return departmentFacade.save(department);
    }

    @PutMapping
    public DepartmentDTO updateDepartment(@RequestBody @Valid DepartmentDTO department) {
        return departmentFacade.update(department);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity deleteDepartment(@PathVariable Long id) {
        departmentFacade.deleteById(id);
        return ResponseEntity.noContent().build();
    }
}
