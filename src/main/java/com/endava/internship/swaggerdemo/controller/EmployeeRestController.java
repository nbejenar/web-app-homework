package com.endava.internship.swaggerdemo.controller;

import com.endava.internship.swaggerdemo.facade.EmployeeFacade;
import com.endava.internship.swaggerdemo.model.dto.EmployeeDTO;
import com.endava.internship.swaggerdemo.model.dto.Identifiable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/v1.0/employee")
public class EmployeeRestController {

    private final EmployeeFacade employeeFacade;

    public EmployeeRestController(EmployeeFacade employeeFacade) {
        this.employeeFacade = employeeFacade;
    }

    @GetMapping
    public ResponseEntity<List<Identifiable<Long>>> getAllEmployees(@RequestParam(required = false) boolean expand) {
        return ResponseEntity.ok(employeeFacade.findAll(expand));
    }

    @GetMapping("/{id}")
    public ResponseEntity<Identifiable<Long>> getEmployee(@PathVariable Long id,
                                                          @RequestParam(required = false) boolean expand) {
        Identifiable<Long> employee = employeeFacade.findById(id, expand);
        return ResponseEntity.ok(employee);
    }

    @PostMapping
    public EmployeeDTO addEmployee(@RequestBody @Valid EmployeeDTO employee) {
        return employeeFacade.save(employee);
    }

    @PutMapping
    public EmployeeDTO updateEmployee(@RequestBody @Valid EmployeeDTO employee) {
        return employeeFacade.update(employee);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity deleteEmployee(@PathVariable Long id) {
        employeeFacade.deleteById(id);
        return ResponseEntity.noContent().build();
    }
}
