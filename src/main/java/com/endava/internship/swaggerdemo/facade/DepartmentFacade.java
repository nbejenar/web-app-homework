package com.endava.internship.swaggerdemo.facade;

import com.endava.internship.swaggerdemo.exceptions.DepartmentNotFoundException;
import com.endava.internship.swaggerdemo.exceptions.EmployeeNotFoundException;
import com.endava.internship.swaggerdemo.exceptions.LocationNotFoundException;
import com.endava.internship.swaggerdemo.model.Department;
import com.endava.internship.swaggerdemo.model.dto.DepartmentDTO;
import com.endava.internship.swaggerdemo.model.dto.DepartmentOnlyName;
import com.endava.internship.swaggerdemo.model.dto.Identifiable;
import com.endava.internship.swaggerdemo.repository.EmployeeRepository;
import com.endava.internship.swaggerdemo.repository.LocationRepository;
import com.endava.internship.swaggerdemo.service.DepartmentService;
import org.springframework.stereotype.Component;

import java.util.List;

import static java.util.stream.Collectors.toList;

@Component
public class DepartmentFacade {

    private final DepartmentService departmentService;
    private final EmployeeRepository employeeRepository;
    private final LocationRepository locationRepository;

    public DepartmentFacade(DepartmentService departmentService,
                            EmployeeRepository employeeRepository,
                            LocationRepository locationRepository) {
        this.departmentService = departmentService;
        this.employeeRepository = employeeRepository;
        this.locationRepository = locationRepository;
    }

    public List<Identifiable<Long>> findAll(boolean expand) {
        return departmentService.findAll().stream()
                .map(d -> expand ? new DepartmentDTO(d) : new DepartmentOnlyName(d))
                .collect(toList());

    }

    public Identifiable<Long> findById(Long id, boolean expand) {
        return departmentService.findById(id)
                .map(d -> expand ? new DepartmentDTO(d) : new DepartmentOnlyName(d))
                .orElseThrow(() -> new DepartmentNotFoundException(id));
    }

    public DepartmentDTO save(DepartmentDTO department) {
        Department d = mapDepartment(department);
        d.setId(null);

        d = departmentService.save(d);

        return new DepartmentDTO(d);
    }


    public DepartmentDTO update(DepartmentDTO department) {
        Department d = mapDepartment(department);

        d = departmentService.save(d);

        return new DepartmentDTO(d);
    }

    public void deleteById(Long id) {
        departmentService.deleteById(id);
    }

    private Department mapDepartment(DepartmentDTO department) {
        Department d = department.toEntity();
        d.setManager(department.getManagerId() == null ? null : employeeRepository
                .findById(department.getManagerId())
                .orElseThrow(() -> new EmployeeNotFoundException(department.getManagerId())));

        d.setLocation(department.getLocationId() == null ? null : locationRepository
                .findById(department.getLocationId())
                .orElseThrow(() -> new LocationNotFoundException(department.getLocationId())));
        return d;
    }
}
