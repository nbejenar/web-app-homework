package com.endava.internship.swaggerdemo.facade;

import com.endava.internship.swaggerdemo.exceptions.DepartmentNotFoundException;
import com.endava.internship.swaggerdemo.exceptions.EmployeeNotFoundException;
import com.endava.internship.swaggerdemo.exceptions.JobNotFoundException;
import com.endava.internship.swaggerdemo.model.Employee;
import com.endava.internship.swaggerdemo.model.dto.EmployeeDTO;
import com.endava.internship.swaggerdemo.model.dto.EmployeeOnlyNameDepartment;
import com.endava.internship.swaggerdemo.model.dto.Identifiable;
import com.endava.internship.swaggerdemo.repository.DepartmentRepository;
import com.endava.internship.swaggerdemo.repository.EmployeeRepository;
import com.endava.internship.swaggerdemo.repository.JobRepository;
import com.endava.internship.swaggerdemo.service.EmployeeService;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static java.util.stream.Collectors.toList;

@Component
public class EmployeeFacade {

    private final EmployeeService employeeService;
    private final EmployeeRepository employeeRepository;
    private final DepartmentRepository departmentRepository;
    private final JobRepository jobRepository;

    public EmployeeFacade(EmployeeService employeeService,
                          EmployeeRepository employeeRepository,
                          DepartmentRepository departmentRepository,
                          JobRepository jobRepository) {
        this.employeeService = employeeService;
        this.employeeRepository = employeeRepository;
        this.departmentRepository = departmentRepository;
        this.jobRepository = jobRepository;
    }

    public List<Identifiable<Long>> findAll(boolean expand) {
        return employeeService.findAll().stream()
                .map(e -> expand ? new EmployeeDTO(e) : new EmployeeOnlyNameDepartment(e))
                .collect(toList());

    }

    public Identifiable<Long> findById(Long id, boolean expand) {
        return employeeService.findById(id)
                .map(e -> expand ? new EmployeeDTO(e) : new EmployeeOnlyNameDepartment(e))
                .orElseThrow(() -> new EmployeeNotFoundException(id));
    }

    @Transactional
    public EmployeeDTO save(EmployeeDTO employee) {
        Employee e = mapEmployee(employee);
        e.setId(null);

        e = employeeService.save(e);

        return new EmployeeDTO(e);
    }

    @Transactional
    public EmployeeDTO update(EmployeeDTO employee) {
        Employee e = mapEmployee(employee);

        e = employeeService.save(e);

        return new EmployeeDTO(e);
    }

    public void deleteById(Long id) {
        employeeService.deleteById(id);
    }

    private Employee mapEmployee(EmployeeDTO employee) {
        Employee e = employee.toEntity();
        e.setDepartment(employee.getDepartmentId() == null ? null : departmentRepository
                .findById(employee.getDepartmentId())
                .orElseThrow(() -> new DepartmentNotFoundException(employee.getDepartmentId())));

        e.setManager(employee.getManagerId() == null ? null : employeeRepository
                .findById(employee.getManagerId())
                .orElseThrow(() -> new EmployeeNotFoundException(employee.getManagerId())));
        e.setJob(employee.getJobId() == null ? null : jobRepository
                .findById(employee.getJobId())
                .orElseThrow(() -> new JobNotFoundException(employee.getJobId())));
        return e;
    }

}
