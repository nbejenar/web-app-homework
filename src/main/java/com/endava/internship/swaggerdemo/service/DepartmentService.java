package com.endava.internship.swaggerdemo.service;

import com.endava.internship.swaggerdemo.model.Department;

import java.util.List;
import java.util.Optional;

public interface DepartmentService {

    List<Department> findAll();

    Optional<Department> findById(Long id);

    Department save(Department employee);

    void deleteById(Long id);
}
