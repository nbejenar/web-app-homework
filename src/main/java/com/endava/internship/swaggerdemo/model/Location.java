package com.endava.internship.swaggerdemo.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "locations")
@Builder
public class Location {

    @Id
    @GeneratedValue(generator = "LOCATIONS_SEQ")
    @SequenceGenerator(name = "LOCATIONS_SEQ", sequenceName = "LOCATIONS_SEQ", allocationSize = 100)
    @Column(name = "location_id")
    private Integer id;

    private String streetAddress;

    private String postalCode;

    private String city;

    private String stateProvince;

    @ManyToOne
    @JoinColumn(name = "country_id")
    private Country country;
}
