package com.endava.internship.swaggerdemo.model.dto;

import com.endava.internship.swaggerdemo.model.Department;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class DepartmentDTO implements Identifiable<Long> {

    private Long id;
    @NotNull
    @NotEmpty
    @NotBlank
    private String departmentName;
    private Long managerId;
    private Integer locationId;

    public DepartmentDTO(Department department) {
        this.id = department.getId();
        this.departmentName = department.getDepartmentName();
        this.managerId = initManagerId(department);
        this.locationId = initLocationId(department);
    }

    private Integer initLocationId(Department department) {
        return department.getLocation() == null ? null : department.getLocation().getId();
    }

    private Long initManagerId(Department department) {
        return department.getManager() == null ? null : department.getManager().getId();
    }

    public Department toEntity() {
        Department department = new Department();
        department.setId(this.id);
        department.setDepartmentName(this.departmentName);

        return department;
    }
}
