package com.endava.internship.swaggerdemo.model;

import com.endava.internship.swaggerdemo.model.Employee;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.io.Serializable;
import java.time.LocalDate;

@Embeddable
@Data
@AllArgsConstructor
@NoArgsConstructor
public class JobHistoryId implements Serializable {

    @ManyToOne
    @Cascade({CascadeType.PERSIST,CascadeType.MERGE,CascadeType.REFRESH,CascadeType.SAVE_UPDATE})
    @JoinColumn(name = "employee_id")
    private Employee employee;

    private LocalDate startDate;
}
