package com.endava.internship.swaggerdemo.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

@Entity
@Table(name = "employees")
@DynamicUpdate
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder(toBuilder=true)
public class Employee implements Cloneable {

    @Id
    @GeneratedValue(generator = "employees_seq")
    @SequenceGenerator(name="employees_seq", sequenceName = "EMPLOYEES_SEQ", allocationSize = 1)
    @Column(name = "employee_id")
    private Long id;

    private String firstName;

    private String lastName;

    private String email;

    private String phoneNumber;

    private LocalDate hireDate;

    @Column(precision = 8, scale = 2, columnDefinition = "numeric")
    private BigDecimal salary;

    @Column(precision = 2, scale = 2, columnDefinition = "numeric")
    private BigDecimal commissionPct;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
    @JoinColumn(name = "job_id")
    private Job job;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
    @JoinColumn(name = "manager_id")
    private Employee manager;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
    @JoinColumn(name = "department_id")
    private Department department;

    @OneToMany(mappedBy = "id.employee", cascade = CascadeType.ALL)
    private List<JobHistory> jobHistoryList = new ArrayList<>();

    @Override
    public String toString() {
        String jb = job == null ? null : job.getJobTitle();
        String dptm = department == null ? null : department.getDepartmentName();
        String mgr = manager == null ? null : manager.getFirstName() + " " + manager.getLastName();
        return "Employee{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", hireDate=" + hireDate +
                ", job=" + jb +
                ", salary=" + salary +
                ", manager=" + mgr +
                ", department=" + dptm +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Employee employee = (Employee) o;
        return Objects.equals(id, employee.id) &&
                Objects.equals(firstName, employee.firstName) &&
                Objects.equals(lastName, employee.lastName) &&
                Objects.equals(email, employee.email) &&
                Objects.equals(phoneNumber, employee.phoneNumber) &&
                Objects.equals(hireDate, employee.hireDate) &&
                Objects.equals(salary, employee.salary) &&
                Objects.equals(commissionPct, employee.commissionPct);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, firstName, lastName, email, phoneNumber, hireDate, salary, commissionPct);
    }

    public Map<String, Object> toMap() {
        Object departmentId = department == null ? null : department.getId();
        Object managerId = manager == null? null : manager.getId();
        Object jobId = job == null? null : job.getId();

        Map<String, Object> values = new HashMap<>();
        values.put("employee_id", null);
        values.put("first_name", firstName);
        values.put("last_name", lastName);
        values.put("salary",salary );
        values.put("phone_number", phoneNumber);
        values.put("hire_date", hireDate);
        values.put("email", email);
        values.put("commission_pct", commissionPct);
        values.put("department_id", departmentId);
        values.put("manager_id", managerId);
        values.put("job_id", jobId);

        return values;
    }
}
