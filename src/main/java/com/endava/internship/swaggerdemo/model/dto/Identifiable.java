package com.endava.internship.swaggerdemo.model.dto;

import java.io.Serializable;

public interface Identifiable<T> extends Serializable {

    T getId();
}
