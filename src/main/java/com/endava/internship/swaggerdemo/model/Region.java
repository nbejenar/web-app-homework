package com.endava.internship.swaggerdemo.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Data
@NoArgsConstructor
@Table(name = "regions")
public class Region {

    @Id
    @Column(name = "region_id")
    private Long id;

    private String regionName;
}
