package com.endava.internship.swaggerdemo.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "jobs")
@Builder
public class Job {

    @Id
    @Column(name = "job_id")
    private String id;

    private String jobTitle;

    private BigDecimal minSalary;

    private BigDecimal maxSalary;

    public Job(String title, BigDecimal minSalary, BigDecimal maxSalary) {
        this.jobTitle = title;
        this.minSalary = minSalary;
        this.maxSalary = maxSalary;
    }

    @Override
    public String toString() {
        return "Job{" +
                "title='" + jobTitle + '\'' +
                '}';
    }
}
