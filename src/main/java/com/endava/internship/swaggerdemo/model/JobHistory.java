package com.endava.internship.swaggerdemo.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.time.LocalDate;

@Entity
@NoArgsConstructor
@Data
public class JobHistory {

    @EmbeddedId
    private JobHistoryId id;

    private LocalDate endDate;

    @ManyToOne
    @Cascade(CascadeType.PERSIST)
    @JoinColumn(name = "job_id")
    private Job job;

    @ManyToOne
    @Cascade(CascadeType.PERSIST)
    @JoinColumn(name = "department_id")
    private Department department;

    public JobHistory(Employee employee, LocalDate startDate, LocalDate endDate, Job job, Department department) {
        this.id = new JobHistoryId(employee, startDate);
        this.endDate = endDate;
        this.job = job;
        this.department = department;
    }

    @Override
    public String toString() {
        return "JobHistory{" +
                "\n endDate=" + endDate +
                "\n job=" + job +
                "\n department=" + department +
                '}';
    }
}
