package com.endava.internship.swaggerdemo.model.dto;

import com.endava.internship.swaggerdemo.model.Employee;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class EmployeeOnlyNameDepartment implements Identifiable<Long> {

    @JsonIgnore
    private Long id;

    private String firstName;

    private String lastName;

    private Long departmentId;

    public EmployeeOnlyNameDepartment(Employee employee) {
        this.id = employee.getId();
        this.firstName = employee.getFirstName();
        this.lastName = employee.getLastName();
        this.departmentId = getDepartmentId(employee);
    }

    private Long getDepartmentId(Employee employee) {
        return employee.getDepartment() == null ? null : employee.getDepartment().getId();
    }

}
