package com.endava.internship.swaggerdemo.model.dto;

import com.endava.internship.swaggerdemo.model.Employee;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Email;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.math.BigDecimal;
import java.time.LocalDate;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class EmployeeDTO implements Identifiable<Long> {

    private Long id;

    @NotNull
    @NotEmpty
    @NotBlank
    private String firstName;

    @NotNull
    @NotEmpty
    @NotBlank
    private String lastName;

    @Email
    private String email;

    @Pattern(regexp = "0\\d{8}", message = "number pattern: 9 digits starting with 0")
    private String phoneNumber;

    private LocalDate hireDate;

    @Min(value = 1)
    private BigDecimal salary;

    private BigDecimal commissionPct;
    private String jobId;
    private Long managerId;
    private Long departmentId;

    public EmployeeDTO(Employee employee) {
        this.id = employee.getId();
        this.firstName = employee.getFirstName();
        this.lastName = employee.getLastName();
        this.email = employee.getEmail();
        this.phoneNumber = employee.getPhoneNumber();
        this.hireDate = employee.getHireDate();
        this.salary = employee.getSalary();
        this.commissionPct = employee.getCommissionPct();
        this.jobId = getJobId(employee);
        this.managerId = getManagerId(employee);
        this.departmentId = getDepartmentId(employee);
    }

    private String getJobId(Employee employee) {
        return employee.getJob() == null ? null : employee.getJob().getId();
    }

    private Long getManagerId(Employee employee) {
        return employee.getManager() == null ? null : employee.getManager().getId();
    }

    private Long getDepartmentId(Employee employee) {
        return employee.getDepartment() == null ? null : employee.getDepartment().getId();
    }

    public Employee toEntity() {
        Employee employee = new Employee();
        employee.setId(this.id);
        employee.setFirstName(this.firstName);
        employee.setLastName(this.lastName);
        employee.setEmail(this.email);
        employee.setPhoneNumber(this.phoneNumber);
        employee.setHireDate(this.hireDate);
        employee.setSalary(this.salary);
        employee.setCommissionPct(this.commissionPct);
        return employee;
    }
}
