package com.endava.internship.swaggerdemo.model.dto;

import com.endava.internship.swaggerdemo.model.Department;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class DepartmentOnlyName implements Identifiable<Long> {

    @JsonIgnore
    private Long id;
    private String name;

    public DepartmentOnlyName(Department d) {
        this.id = d.getId();
        this.name = d.getDepartmentName();
    }
}
