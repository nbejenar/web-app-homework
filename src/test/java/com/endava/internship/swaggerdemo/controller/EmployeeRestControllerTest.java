package com.endava.internship.swaggerdemo.controller;

import com.endava.internship.swaggerdemo.exceptions.EmployeeNotFoundException;
import com.endava.internship.swaggerdemo.facade.EmployeeFacade;
import com.endava.internship.swaggerdemo.model.dto.EmployeeDTO;
import com.endava.internship.swaggerdemo.model.dto.Identifiable;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.nio.charset.StandardCharsets;
import java.util.List;

import static com.endava.internship.swaggerdemo.controller.TestControllerUtils.VALID_EMPLOYEE_DTO_SAVED;
import static com.endava.internship.swaggerdemo.controller.TestControllerUtils.VALID_EMPLOYEE_DTO_TO_SAVE;
import static com.endava.internship.swaggerdemo.controller.TestControllerUtils.VALID_EMPLOYEE_DTO_UPDATE;
import static com.endava.internship.swaggerdemo.util.TestUtils.ENDPOINT;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest(controllers = EmployeeRestController.class)
class EmployeeRestControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    EmployeeFacade employeeFacade;

    @ParameterizedTest
    @MethodSource("com.endava.internship.swaggerdemo.controller.TestControllerUtils#getAllEmployeesVariants")
    void testGetAllEmployees(boolean expand, List<Identifiable<Long>> employees) throws Exception {
        given(employeeFacade.findAll(expand))
                .willReturn(employees);

        MvcResult mvcResult = mockMvc.perform(get(ENDPOINT + "employee")
                .queryParam("expand", String.valueOf(expand))
                .contentType("application/json"))
                .andExpect(status().is2xxSuccessful())
                .andReturn();

        String actualResponse = mvcResult.getResponse().getContentAsString(StandardCharsets.UTF_8);

        verify(employeeFacade).findAll(expand);
        assertThat(actualResponse).isEqualToIgnoringWhitespace(objectMapper.writeValueAsString(employees));

    }

    @Test
    void testGetEmployee_shouldThrowEmployeeNotFoundException() throws Exception {
        given(employeeFacade.findById(anyLong(), anyBoolean()))
                .willThrow(new EmployeeNotFoundException(anyLong()));

        mockMvc.perform(get(ENDPOINT + "employee/{id}", anyLong())
                .contentType("application/json"))
                .andExpect(status().isNotFound())
                .andExpect(result -> assertThat(result.getResolvedException()).isInstanceOf(EmployeeNotFoundException.class));
    }

    @ParameterizedTest
    @MethodSource("com.endava.internship.swaggerdemo.controller.TestControllerUtils#employeeControllerFindArgs")
    void testGetEmployee(boolean expand, Identifiable<Long> employee) throws Exception {
        given(employeeFacade.findById(10L, expand))
                .willReturn(employee);

        MvcResult mvcResult = mockMvc.perform(get(ENDPOINT + "employee/{id}", 10L)
                .queryParam("expand", String.valueOf(expand))
                .contentType("application/json"))
                .andExpect(status().is2xxSuccessful())
                .andReturn();

        String actualResponse = mvcResult.getResponse().getContentAsString();

        assertThat(actualResponse).isEqualToIgnoringWhitespace(objectMapper.writeValueAsString(employee));
        verify(employeeFacade).findById(10L, expand);
    }

    @ParameterizedTest
    @MethodSource("com.endava.internship.swaggerdemo.controller.TestControllerUtils#badRequestEmployees")
    void testAddEmployee_whenNotValidated_thenBadRequest(EmployeeDTO employee) throws Exception {
        mockMvc.perform(post(ENDPOINT + "employee")
                .content(objectMapper.writeValueAsString(employee))
                .contentType("application/json"))
                .andExpect(status().isBadRequest())
                .andReturn();
    }

    @ParameterizedTest
    @MethodSource("com.endava.internship.swaggerdemo.controller.TestControllerUtils#getEmployeeControllerPossibleCustomExceptions")
    void testAddEmployee_shouldThrowCustomException(Class<? extends RuntimeException> exception) throws Exception {
        given(employeeFacade.save(any()))
                .willThrow(exception);

        mockMvc.perform(post(ENDPOINT + "employee")
                .content(objectMapper.writeValueAsString(VALID_EMPLOYEE_DTO_TO_SAVE))
                .contentType("application/json"))
                .andExpect(status().isNotFound())
                .andExpect(result -> assertThat(result.getResolvedException()).isInstanceOf(exception));
    }

    @Test
    void testAddEmployee() throws Exception {
        given(employeeFacade.save(VALID_EMPLOYEE_DTO_TO_SAVE))
                .willReturn(VALID_EMPLOYEE_DTO_SAVED);

        MvcResult mvcResult = mockMvc.perform(post(ENDPOINT + "employee")
                .content(objectMapper.writeValueAsString(VALID_EMPLOYEE_DTO_TO_SAVE))
                .contentType("application/json"))
                .andExpect(status().is2xxSuccessful())
                .andReturn();

        String actualResponse = mvcResult.getResponse().getContentAsString();

        assertThat(actualResponse).isEqualToIgnoringWhitespace(objectMapper.writeValueAsString(VALID_EMPLOYEE_DTO_SAVED));
        verify(employeeFacade).save(VALID_EMPLOYEE_DTO_TO_SAVE);
    }

    @ParameterizedTest
    @MethodSource("com.endava.internship.swaggerdemo.controller.TestControllerUtils#badRequestEmployees")
    void testUpdateEmployee_whenNotValidated_thenBadRequest(EmployeeDTO employee) throws Exception {
        mockMvc.perform(put(ENDPOINT + "employee")
                .content(objectMapper.writeValueAsString(employee))
                .contentType("application/json"))
                .andExpect(status().isBadRequest())
                .andReturn();
    }

    @ParameterizedTest
    @MethodSource("com.endava.internship.swaggerdemo.controller.TestControllerUtils#getEmployeeControllerPossibleCustomExceptions")
    void testUpdateEmployee_shouldThrowCustomException(Class<? extends RuntimeException> exception) throws Exception {
        given(employeeFacade.update(any()))
                .willThrow(exception);

        mockMvc.perform(put(ENDPOINT + "employee")
                .content(objectMapper.writeValueAsString(VALID_EMPLOYEE_DTO_UPDATE))
                .contentType("application/json"))
                .andExpect(status().isNotFound())
                .andExpect(result -> assertThat(result.getResolvedException()).isInstanceOf(exception));
    }

    @Test
    void testUpdateEmployee() throws Exception {
        given(employeeFacade.update(VALID_EMPLOYEE_DTO_UPDATE))
                .willReturn(VALID_EMPLOYEE_DTO_UPDATE);

        MvcResult mvcResult = mockMvc.perform(put(ENDPOINT + "employee")
                .content(objectMapper.writeValueAsString(VALID_EMPLOYEE_DTO_UPDATE))
                .contentType("application/json"))
                .andExpect(status().is2xxSuccessful())
                .andReturn();

        String actualResponse = mvcResult.getResponse().getContentAsString();

        assertThat(actualResponse).isEqualToIgnoringWhitespace(objectMapper.writeValueAsString(VALID_EMPLOYEE_DTO_UPDATE));
        verify(employeeFacade).update(VALID_EMPLOYEE_DTO_UPDATE);
    }

    @Test
    void testDeleteEmployee_shouldThrowEmployeeNotFoundException() throws Exception {
        doThrow(EmployeeNotFoundException.class)
                .when(employeeFacade)
                .deleteById(anyLong());

        mockMvc.perform(delete(ENDPOINT + "employee/{id}", anyLong())
                .contentType("application/json"))
                .andExpect(status().isNotFound())
                .andExpect(result -> assertThat(result.getResolvedException()).isInstanceOf(EmployeeNotFoundException.class));
    }

    @Test
    void testDeleteEmployee() throws Exception {
        mockMvc.perform(delete(ENDPOINT + "employee/{id}", anyLong())
                .contentType("application/json"))
                .andExpect(status().isNoContent())
                .andExpect(content().string(""));
    }
}