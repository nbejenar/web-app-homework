package com.endava.internship.swaggerdemo.controller;

import com.endava.internship.swaggerdemo.exceptions.DepartmentNotFoundException;
import com.endava.internship.swaggerdemo.exceptions.EmployeeNotFoundException;
import com.endava.internship.swaggerdemo.exceptions.JobNotFoundException;
import com.endava.internship.swaggerdemo.exceptions.LocationNotFoundException;
import com.endava.internship.swaggerdemo.model.dto.DepartmentDTO;
import com.endava.internship.swaggerdemo.model.dto.DepartmentOnlyName;
import com.endava.internship.swaggerdemo.model.dto.EmployeeDTO;
import com.endava.internship.swaggerdemo.model.dto.EmployeeOnlyNameDepartment;
import org.junit.jupiter.params.provider.Arguments;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;
import java.util.stream.Stream;

import static java.util.Collections.emptyList;

public class TestControllerUtils {

    public static final EmployeeDTO VALID_EMPLOYEE_DTO_TO_SAVE = EmployeeDTO.builder()
            .email("example@site.com")
            .firstName("Valid")
            .lastName("Name")
            .phoneNumber("068242375")
            .hireDate(LocalDate.now())
            .salary(BigDecimal.valueOf(10000))
            .build();

    public static final EmployeeDTO VALID_EMPLOYEE_DTO_SAVED = EmployeeDTO.builder()
            .email("example@site.com")
            .firstName("Valid")
            .lastName("Name")
            .phoneNumber("068242375")
            .hireDate(LocalDate.now())
            .salary(BigDecimal.valueOf(10000))
            .id(10L)
            .build();

    public static final EmployeeDTO VALID_EMPLOYEE_DTO_UPDATE = VALID_EMPLOYEE_DTO_SAVED;

    public static final DepartmentDTO VALID_DEPARTMENT_DTO_TO_SAVE = DepartmentDTO.builder()
            .departmentName("a dep")
            .build();

    public static final DepartmentDTO VALID_DEPARTMENT_DTO_SAVED = DepartmentDTO.builder()
            .departmentName("a dep")
            .id(10L)
            .build();

    public static final DepartmentDTO VALID_DEPARTMENT_DTO_UPDATE = VALID_DEPARTMENT_DTO_SAVED;


    public static Stream<Arguments> getAllEmployeesVariants() {
        return Stream.of(
                Arguments.of(true, List.of(new EmployeeDTO())),
                Arguments.of(false, List.of(new EmployeeOnlyNameDepartment())),
                Arguments.of(true, emptyList())
        );
    }

    public static Stream<Arguments> getAllDepartmentsVariants() {
        return Stream.of(
                Arguments.of(true, List.of(new DepartmentDTO())),
                Arguments.of(false, List.of(new DepartmentOnlyName())),
                Arguments.of(true, emptyList())
        );
    }

    public static Stream<Arguments> employeeControllerFindArgs() {
        return Stream.of(
                Arguments.of(true, new EmployeeDTO()),
                Arguments.of(false, new EmployeeOnlyNameDepartment())
        );
    }

    public static Stream<Arguments> departmentControllerFindArgs() {
        return Stream.of(
                Arguments.of(true, new DepartmentDTO()),
                Arguments.of(false, new DepartmentOnlyName())
        );
    }

    public static Stream<Arguments> badRequestEmployees() {
        EmployeeDTO badSalary = EmployeeDTO.builder()
                .email("example@site.com")
                .firstName("Valid")
                .lastName("Name")
                .phoneNumber("068242375")
                .salary(BigDecimal.valueOf(0))
                .build();

        EmployeeDTO badEmail = EmployeeDTO.builder()
                .email("examplesite.com")
                .firstName("Valid")
                .lastName("Name")
                .phoneNumber("068242375")
                .salary(BigDecimal.valueOf(10000))
                .build();

        EmployeeDTO badFirstName = EmployeeDTO.builder()
                .email("example@site.com")
                .firstName("  ")
                .lastName("Name")
                .phoneNumber("068242375")
                .salary(BigDecimal.valueOf(10000))
                .build();

        EmployeeDTO badLastName = EmployeeDTO.builder()
                .email("example@site.com")
                .firstName("Valid")
                .lastName(null)
                .phoneNumber("068242375")
                .salary(BigDecimal.valueOf(10000))
                .build();

        EmployeeDTO badPhone = EmployeeDTO.builder()
                .email("example@site.com")
                .firstName("Valid")
                .lastName("Name")
                .phoneNumber("+068242375")
                .salary(BigDecimal.valueOf(10000))
                .build();

        return Stream.of(
                Arguments.of(badEmail),
                Arguments.of(badFirstName),
                Arguments.of(badLastName),
                Arguments.of(badPhone),
                Arguments.of(badSalary)
        );
    }

    public static Stream<Arguments> badRequestDepartments() {
        DepartmentDTO badName = DepartmentDTO.builder()
                .departmentName("")
                .build();

        return Stream.of(
                Arguments.of(badName)
        );
    }

    public static final Stream<Arguments> getEmployeeControllerPossibleCustomExceptions() {
        return Stream.of(
                Arguments.of(DepartmentNotFoundException.class),
                Arguments.of(EmployeeNotFoundException.class),
                Arguments.of(JobNotFoundException.class)
        );
    }

    public static final Stream<Arguments> getEmployeeController_DtoWithNotFoundIds() {
        EmployeeDTO badJobId = EmployeeDTO.builder()
                .email("example@site.com")
                .firstName("Valid")
                .lastName("Name")
                .phoneNumber("068242375")
                .salary(BigDecimal.valueOf(10000))
                .jobId("BRUH")
                .build();

        EmployeeDTO badManagerId = EmployeeDTO.builder()
                .email("example@site.com")
                .firstName("Valid")
                .lastName("Name")
                .phoneNumber("068242375")
                .salary(BigDecimal.valueOf(10000))
                .managerId(-10L)
                .build();

        EmployeeDTO badDepartmentId = EmployeeDTO.builder()
                .email("example@site.com")
                .firstName("Valid")
                .lastName("Name")
                .phoneNumber("068242375")
                .salary(BigDecimal.valueOf(10000))
                .departmentId(-10L)
                .build();

        return Stream.of(
                Arguments.of(badDepartmentId, DepartmentNotFoundException.class),
                Arguments.of(badManagerId, EmployeeNotFoundException.class),
                Arguments.of(badJobId, JobNotFoundException.class)
        );
    }

    public static final Stream<Arguments> getDepartmentControllerPossibleCustomExceptions() {
        return Stream.of(
                Arguments.of(LocationNotFoundException.class),
                Arguments.of(EmployeeNotFoundException.class)
        );
    }
}
