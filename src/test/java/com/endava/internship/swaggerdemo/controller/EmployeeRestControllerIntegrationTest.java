package com.endava.internship.swaggerdemo.controller;

import com.endava.internship.swaggerdemo.exceptions.EmployeeNotFoundException;
import com.endava.internship.swaggerdemo.exceptions.ErrorResponse;
import com.endava.internship.swaggerdemo.facade.EmployeeFacade;
import com.endava.internship.swaggerdemo.model.dto.EmployeeDTO;
import com.endava.internship.swaggerdemo.model.dto.Identifiable;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.ClassPathResource;
import org.springframework.jdbc.datasource.init.ScriptUtils;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.transaction.annotation.Transactional;

import javax.sql.DataSource;
import java.nio.charset.StandardCharsets;
import java.sql.Connection;
import java.sql.SQLException;

import static com.endava.internship.swaggerdemo.controller.TestControllerUtils.VALID_EMPLOYEE_DTO_TO_SAVE;
import static com.endava.internship.swaggerdemo.util.TestUtils.ENDPOINT;
import static com.endava.internship.swaggerdemo.util.TestUtils.EXISTENT_EMPLOYEE_ID;
import static com.endava.internship.swaggerdemo.util.TestUtils.EXPECTED_FIND_BY_ID_100;
import static com.endava.internship.swaggerdemo.util.TestUtils.EXPECTED_FIND_BY_ID_100_EXPANDED;
import static com.endava.internship.swaggerdemo.util.TestUtils.EXPECTED_FIND_BY_ID_100_TO_UPDATE;
import static com.endava.internship.swaggerdemo.util.TestUtils.GET_ALL_COUNT;
import static com.endava.internship.swaggerdemo.util.TestUtils.NON_EXISTENT_EMPLOYEE_ID;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles({"test", "jdbc"})
@TestPropertySource(locations = "classpath:application-test.properties")
@Transactional
@Slf4j
class EmployeeRestControllerIntegrationTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    EmployeeFacade employeeMapper;

    @BeforeAll
    static void setup(@Autowired DataSource dataSource) throws SQLException {
        try (Connection conn = dataSource.getConnection()) {
            conn.setAutoCommit(false);
            ScriptUtils.executeSqlScript(conn, new ClassPathResource("test-dump.sql"));
            conn.commit();
            log.info("||||||||||||||||||| populated h2 db with test data ||||||||||||||||||||");
        } catch (SQLException e) {
            log.error(e.getMessage(), e);
            throw e;
        }
    }

    @ParameterizedTest
    @MethodSource("com.endava.internship.swaggerdemo.util.TestUtils#badFindUserByIdRequests")
    void shouldNotFindUser(Object id, ErrorResponse expectedResponse, String lang) throws Exception {
        MvcResult mvcResult = mockMvc.perform(get(ENDPOINT + "employee/{id}", id)
                .contentType("application/json")
                .header("Accept-Language", lang))
                .andExpect(status().is(expectedResponse.getStatus()))
                .andReturn();

        String actualResponse = mvcResult.getResponse().getContentAsString(StandardCharsets.UTF_8);

        assertThat(actualResponse).isEqualToIgnoringWhitespace(objectMapper.writeValueAsString(expectedResponse));
    }

    @ParameterizedTest
    @ValueSource(booleans = {true, false})
    void shouldFindUser(boolean expand) throws Exception {
        Long id = 100L;

        MvcResult mvcResult = mockMvc.perform(get(ENDPOINT + "employee/{id}", id)
                .queryParam("expand", String.valueOf(expand))
                .contentType("application/json")
                .header("Accept-Language", "en"))
                .andExpect(status().is2xxSuccessful())
                .andReturn();

        Identifiable<Long> expectedResponse = expand ? EXPECTED_FIND_BY_ID_100_EXPANDED : EXPECTED_FIND_BY_ID_100;
        String actualResponse = mvcResult.getResponse().getContentAsString();

        assertThat(actualResponse).isEqualToIgnoringWhitespace(objectMapper.writeValueAsString(expectedResponse));
    }

    @RepeatedTest(3)
    void testGetAll() throws Exception {
        mockMvc.perform(get(ENDPOINT + "employee")
                .contentType("application/json"))
                .andExpect(status().is2xxSuccessful())
                .andExpect(jsonPath("$", hasSize(GET_ALL_COUNT)));
    }

    @ParameterizedTest
    @MethodSource("com.endava.internship.swaggerdemo.controller.TestControllerUtils#badRequestEmployees")
    void testAddEmployee_whenNotValidated_thenBadRequest(EmployeeDTO employee) throws Exception {
        MvcResult mvcResult = mockMvc.perform(post(ENDPOINT + "employee")
                .content(objectMapper.writeValueAsString(employee))
                .contentType("application/json"))
                .andExpect(status().isBadRequest())
                .andReturn();
    }

    @ParameterizedTest
    @MethodSource("com.endava.internship.swaggerdemo.controller.TestControllerUtils#getEmployeeController_DtoWithNotFoundIds")
    void testAddEmployee_shouldThrowCustomException(EmployeeDTO DtoToSave,
                                                    Class<? extends RuntimeException> exception) throws Exception {
        mockMvc.perform(post(ENDPOINT + "employee")
                .content(objectMapper.writeValueAsString(DtoToSave))
                .contentType("application/json"))
                .andExpect(status().isNotFound())
                .andExpect(result -> assertThat(result.getResolvedException()).isInstanceOf(exception));
    }

    @Test
    void testAddEmployee() throws Exception {
        MvcResult mvcResult = mockMvc.perform(post(ENDPOINT + "employee")
                .content(objectMapper.writeValueAsString(VALID_EMPLOYEE_DTO_TO_SAVE))
                .contentType("application/json"))
                .andExpect(status().is2xxSuccessful())
                .andReturn();

        EmployeeDTO actualResponse = objectMapper
                .readValue(mvcResult.getResponse().getContentAsString(), EmployeeDTO.class);
        Identifiable<Long> expectedResponse = employeeMapper.findById(actualResponse.getId(), true);

        assertThat(actualResponse)
                .usingRecursiveComparison()
                .ignoringFields("id")
                .isEqualTo(VALID_EMPLOYEE_DTO_TO_SAVE);
        assertThat(actualResponse).isEqualTo(expectedResponse);
    }

    @ParameterizedTest
    @MethodSource("com.endava.internship.swaggerdemo.controller.TestControllerUtils#badRequestEmployees")
    void testUpdateEmployee_whenNotValidated_thenBadRequest(EmployeeDTO employee) throws Exception {
        MvcResult mvcResult = mockMvc.perform(put(ENDPOINT + "employee")
                .content(objectMapper.writeValueAsString(employee))
                .contentType("application/json"))
                .andExpect(status().isBadRequest())
                .andReturn();
    }

    @ParameterizedTest
    @MethodSource("com.endava.internship.swaggerdemo.controller.TestControllerUtils#getEmployeeController_DtoWithNotFoundIds")
    void testUpdateEmployee_shouldThrowCustomException(EmployeeDTO DtoToSave,
                                                       Class<? extends RuntimeException> exception) throws Exception {
        mockMvc.perform(put(ENDPOINT + "employee")
                .content(objectMapper.writeValueAsString(DtoToSave))
                .contentType("application/json"))
                .andExpect(status().isNotFound())
                .andExpect(result -> assertThat(result.getResolvedException()).isInstanceOf(exception));
    }

    @Test
    void testUpdateEmployee() throws Exception {
        MvcResult mvcResult = mockMvc.perform(put(ENDPOINT + "employee")
                .content(objectMapper.writeValueAsString(EXPECTED_FIND_BY_ID_100_TO_UPDATE))
                .contentType("application/json"))
                .andExpect(status().is2xxSuccessful())
                .andReturn();

        EmployeeDTO actualResponse = objectMapper
                .readValue(mvcResult.getResponse().getContentAsString(), EmployeeDTO.class);

        assertThat(actualResponse)
                .usingRecursiveComparison()
                .ignoringFields("firstName")
                .isEqualTo(EXPECTED_FIND_BY_ID_100_EXPANDED);
        assertThat(actualResponse.getFirstName()).isNotEqualTo(((EmployeeDTO) EXPECTED_FIND_BY_ID_100_EXPANDED).getFirstName());
        assertThat(actualResponse).isEqualTo(EXPECTED_FIND_BY_ID_100_TO_UPDATE);
    }

    @Test
    void testDeleteEmployee_shouldThrowEmployeeNotFoundException() throws Exception {
        mockMvc.perform(delete(ENDPOINT + "employee/{id}", NON_EXISTENT_EMPLOYEE_ID)
                .contentType("application/json"))
                .andExpect(status().isNotFound())
                .andExpect(result -> assertThat(result.getResolvedException()).isInstanceOf(EmployeeNotFoundException.class));
    }

    @Test
    void testDeleteEmployee() throws Exception {
        mockMvc.perform(delete(ENDPOINT + "employee/{id}", EXISTENT_EMPLOYEE_ID)
                .contentType("application/json"))
                .andExpect(status().is2xxSuccessful())
                .andExpect(content().string(""));

        assertThatThrownBy(() -> {
            employeeMapper.findById(EXISTENT_EMPLOYEE_ID, true);
        }).isInstanceOfAny(EmployeeNotFoundException.class);
    }
}