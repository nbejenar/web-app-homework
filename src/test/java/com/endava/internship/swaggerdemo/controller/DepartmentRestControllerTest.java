package com.endava.internship.swaggerdemo.controller;

import com.endava.internship.swaggerdemo.exceptions.DepartmentNotFoundException;
import com.endava.internship.swaggerdemo.facade.DepartmentFacade;
import com.endava.internship.swaggerdemo.model.dto.DepartmentDTO;
import com.endava.internship.swaggerdemo.model.dto.Identifiable;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.nio.charset.StandardCharsets;
import java.util.List;

import static com.endava.internship.swaggerdemo.controller.TestControllerUtils.VALID_DEPARTMENT_DTO_SAVED;
import static com.endava.internship.swaggerdemo.controller.TestControllerUtils.VALID_DEPARTMENT_DTO_TO_SAVE;
import static com.endava.internship.swaggerdemo.controller.TestControllerUtils.VALID_DEPARTMENT_DTO_UPDATE;
import static com.endava.internship.swaggerdemo.util.TestUtils.ENDPOINT;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest(controllers = DepartmentRestController.class)
class DepartmentRestControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    DepartmentFacade departmentFacade;

    @ParameterizedTest
    @MethodSource("com.endava.internship.swaggerdemo.controller.TestControllerUtils#getAllDepartmentsVariants")
    void testGetAllDepartments(boolean expand, List<Identifiable<Long>> departments) throws Exception {
        given(departmentFacade.findAll(expand))
                .willReturn(departments);

        MvcResult mvcResult = mockMvc.perform(get(ENDPOINT + "department")
                .queryParam("expand", String.valueOf(expand))
                .contentType("application/json"))
                .andExpect(status().is2xxSuccessful())
                .andReturn();

        String actualResponse = mvcResult.getResponse().getContentAsString(StandardCharsets.UTF_8);

        verify(departmentFacade).findAll(expand);
        assertThat(actualResponse).isEqualToIgnoringWhitespace(objectMapper.writeValueAsString(departments));

    }

    @Test
    void testGetDepartment_shouldThrowDepartmentNotFoundException() throws Exception {
        given(departmentFacade.findById(anyLong(), anyBoolean()))
                .willThrow(new DepartmentNotFoundException(anyLong()));

        mockMvc.perform(get(ENDPOINT + "department/{id}", anyLong())
                .contentType("application/json"))
                .andExpect(status().isNotFound())
                .andExpect(result -> assertThat(result.getResolvedException()).isInstanceOf(DepartmentNotFoundException.class));
    }

    @ParameterizedTest
    @MethodSource("com.endava.internship.swaggerdemo.controller.TestControllerUtils#departmentControllerFindArgs")
    void testGetDepartment(boolean expand, Identifiable<Long> department) throws Exception {
        given(departmentFacade.findById(10L, expand))
                .willReturn(department);

        MvcResult mvcResult = mockMvc.perform(get(ENDPOINT + "department/{id}", 10L)
                .queryParam("expand", String.valueOf(expand))
                .contentType("application/json"))
                .andExpect(status().is2xxSuccessful())
                .andReturn();

        Identifiable<Long> expectedValue = department;
        String actualResponse = mvcResult.getResponse().getContentAsString();

        assertThat(actualResponse).isEqualToIgnoringWhitespace(objectMapper.writeValueAsString(expectedValue));
        verify(departmentFacade).findById(10L, expand);
    }

    @ParameterizedTest
    @MethodSource("com.endava.internship.swaggerdemo.controller.TestControllerUtils#badRequestDepartments")
    void testAddDepartment_whenNotValidated_thenBadRequest(DepartmentDTO department) throws Exception {
        MvcResult mvcResult = mockMvc.perform(post(ENDPOINT + "department")
                .content(objectMapper.writeValueAsString(department))
                .contentType("application/json"))
                .andExpect(status().isBadRequest())
                .andReturn();
    }

    @ParameterizedTest
    @MethodSource("com.endava.internship.swaggerdemo.controller.TestControllerUtils#getDepartmentControllerPossibleCustomExceptions")
    void testAddDepartment_shouldThrowCustomException(Class<? extends RuntimeException> exception) throws Exception {
        given(departmentFacade.save(any()))
                .willThrow(exception);

        mockMvc.perform(post(ENDPOINT + "department")
                .content(objectMapper.writeValueAsString(VALID_DEPARTMENT_DTO_TO_SAVE))
                .contentType("application/json"))
                .andExpect(status().isNotFound())
                .andExpect(result -> assertThat(result.getResolvedException()).isInstanceOf(exception));
    }

    @Test
    void testAddDepartment() throws Exception {
        given(departmentFacade.save(VALID_DEPARTMENT_DTO_TO_SAVE))
                .willReturn(VALID_DEPARTMENT_DTO_SAVED);

        MvcResult mvcResult = mockMvc.perform(post(ENDPOINT + "department")
                .content(objectMapper.writeValueAsString(VALID_DEPARTMENT_DTO_TO_SAVE))
                .contentType("application/json"))
                .andExpect(status().is2xxSuccessful())
                .andReturn();

        String actualResponse = mvcResult.getResponse().getContentAsString();

        assertThat(actualResponse).isEqualToIgnoringWhitespace(objectMapper.writeValueAsString(VALID_DEPARTMENT_DTO_SAVED));
        verify(departmentFacade).save(VALID_DEPARTMENT_DTO_TO_SAVE);
    }

    @ParameterizedTest
    @MethodSource("com.endava.internship.swaggerdemo.controller.TestControllerUtils#badRequestDepartments")
    void testUpdateDepartment_whenNotValidated_thenBadRequest(DepartmentDTO employee) throws Exception {
        MvcResult mvcResult = mockMvc.perform(put(ENDPOINT + "department")
                .content(objectMapper.writeValueAsString(employee))
                .contentType("application/json"))
                .andExpect(status().isBadRequest())
                .andReturn();
    }

    @ParameterizedTest
    @MethodSource("com.endava.internship.swaggerdemo.controller.TestControllerUtils#getDepartmentControllerPossibleCustomExceptions")
    void testUpdateDepartment_shouldThrowCustomException(Class<? extends RuntimeException> exception) throws Exception {
        given(departmentFacade.update(any()))
                .willThrow(exception);

        mockMvc.perform(put(ENDPOINT + "department")
                .content(objectMapper.writeValueAsString(VALID_DEPARTMENT_DTO_UPDATE))
                .contentType("application/json"))
                .andExpect(status().isNotFound())
                .andExpect(result -> assertThat(result.getResolvedException()).isInstanceOf(exception));
    }

    @Test
    void testUpdateDepartment() throws Exception {
        given(departmentFacade.update(VALID_DEPARTMENT_DTO_UPDATE))
                .willReturn(VALID_DEPARTMENT_DTO_UPDATE);

        MvcResult mvcResult = mockMvc.perform(put(ENDPOINT + "department")
                .content(objectMapper.writeValueAsString(VALID_DEPARTMENT_DTO_UPDATE))
                .contentType("application/json"))
                .andExpect(status().is2xxSuccessful())
                .andReturn();

        String actualResponse = mvcResult.getResponse().getContentAsString();

        assertThat(actualResponse).isEqualToIgnoringWhitespace(objectMapper.writeValueAsString(VALID_DEPARTMENT_DTO_UPDATE));
        verify(departmentFacade).update(VALID_DEPARTMENT_DTO_UPDATE);
    }

    @Test
    void testDeleteDepartment_shouldThrowDepartmentNotFoundException() throws Exception {
        doThrow(DepartmentNotFoundException.class)
                .when(departmentFacade)
                .deleteById(anyLong());

        mockMvc.perform(delete(ENDPOINT + "department/{id}", anyLong())
                .contentType("application/json"))
                .andExpect(status().isNotFound())
                .andExpect(result -> assertThat(result.getResolvedException()).isInstanceOf(DepartmentNotFoundException.class));
    }

    @Test
    void testDeleteDepartment() throws Exception {
        mockMvc.perform(delete(ENDPOINT + "department/{id}", anyLong())
                .contentType("application/json"))
                .andExpect(status().isNoContent())
                .andExpect(content().string(""));
    }
}