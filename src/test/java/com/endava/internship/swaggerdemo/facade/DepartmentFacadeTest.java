package com.endava.internship.swaggerdemo.facade;

import com.endava.internship.swaggerdemo.exceptions.DepartmentNotFoundException;
import com.endava.internship.swaggerdemo.exceptions.EmployeeNotFoundException;
import com.endava.internship.swaggerdemo.exceptions.LocationNotFoundException;
import com.endava.internship.swaggerdemo.model.Department;
import com.endava.internship.swaggerdemo.model.dto.DepartmentDTO;
import com.endava.internship.swaggerdemo.model.dto.Identifiable;
import com.endava.internship.swaggerdemo.repository.EmployeeRepository;
import com.endava.internship.swaggerdemo.repository.LocationRepository;
import com.endava.internship.swaggerdemo.service.DepartmentService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;
import java.util.Optional;

import static com.endava.internship.swaggerdemo.facade.TestMapperUtils.ANY_EMPLOYEE;
import static com.endava.internship.swaggerdemo.facade.TestMapperUtils.ANY_LOCATION;
import static com.endava.internship.swaggerdemo.facade.TestMapperUtils.DEPARTMENT_DTO_VERIFIED;
import static com.endava.internship.swaggerdemo.facade.TestMapperUtils.DEPARTMENT_DTO_WRONG_IDS;
import static com.endava.internship.swaggerdemo.facade.TestMapperUtils.getDepartmentFromDtoVerified;
import static com.endava.internship.swaggerdemo.util.TestUtils.DEPARTMENTS_DEFAULT;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class DepartmentFacadeTest {

    @Mock
    DepartmentService departmentService;

    @Mock
    EmployeeRepository employeeRepository;

    @Mock
    LocationRepository locationRepository;

    @InjectMocks
    DepartmentFacade departmentFacade;

    @ParameterizedTest
    @MethodSource("com.endava.internship.swaggerdemo.facade.TestMapperUtils#departmentMapperFindAllArgs")
    void testFindAll(boolean expand, List<Identifiable<Long>> expectedList) {
        given(departmentService.findAll())
                .willReturn(DEPARTMENTS_DEFAULT);

        List<Identifiable<Long>> actual = departmentFacade.findAll(expand);

        verify(departmentService).findAll();
        assertThat(actual).isEqualTo(expectedList);
    }

    @ParameterizedTest
    @MethodSource("com.endava.internship.swaggerdemo.facade.TestMapperUtils#departmentMapperFindArgs")
    void testFindById(boolean expand, Class<? extends Identifiable<Long>> clazz) {
        given(departmentService.findById(1L))
                .willReturn(Optional.of(new Department()));

        Identifiable<Long> actual = departmentFacade.findById(1L, expand);

        verify(departmentService).findById(1L);
        assertThat(actual.getClass()).isEqualTo(clazz);
    }

    @Test
    void testFindById_shouldThrowDepartmentNotFoundException() {
        given(departmentService.findById(1L))
                .willReturn(Optional.empty());

        assertThatThrownBy(() -> departmentFacade.findById(1L, true))
                .isInstanceOf(DepartmentNotFoundException.class)
                .hasMessageContaining("Department with id 1 was not found");
    }

    @Test
    void testSave_shouldThrowEmployeeNotFoundException() {
        Long managerId = DEPARTMENT_DTO_WRONG_IDS.getManagerId();

        given(employeeRepository.findById(managerId))
                .willReturn(Optional.empty());

        assertThatThrownBy(() -> departmentFacade.save(DEPARTMENT_DTO_WRONG_IDS))
                .isInstanceOf(EmployeeNotFoundException.class)
                .hasMessageContaining("Employee with id " + managerId + " was not found");
        verify(departmentService, never()).save(any());
    }

    @Test
    void testSave_shouldThrowLocationNotFoundException() {
        Integer locationId = DEPARTMENT_DTO_WRONG_IDS.getLocationId();

        given(employeeRepository.findById(any()))
                .willReturn(Optional.of(ANY_EMPLOYEE));
        given(locationRepository.findById(locationId))
                .willReturn(Optional.empty());

        assertThatThrownBy(() -> departmentFacade.save(DEPARTMENT_DTO_WRONG_IDS))
                .isInstanceOf(LocationNotFoundException.class)
                .hasMessageContaining("Location with id " + locationId + " was not found");
        verify(departmentService, never()).save(any());
    }

    @Test
    void testSave() {
        ArgumentCaptor<Department> argumentCaptor = ArgumentCaptor.forClass(Department.class);

        given(employeeRepository.findById(any()))
                .willReturn(Optional.of(ANY_EMPLOYEE));
        given(locationRepository.findById(any()))
                .willReturn(Optional.of(ANY_LOCATION));

        Department savedDepartment = getDepartmentFromDtoVerified();
        savedDepartment.setId(1000L);
        given(departmentService.save(argumentCaptor.capture()))
                .willReturn(savedDepartment);

        DepartmentDTO actual = departmentFacade.save(DEPARTMENT_DTO_VERIFIED);

        verify(departmentService).save(argumentCaptor.getValue());
        assertThat(argumentCaptor.getValue())
                .usingRecursiveComparison()
                .ignoringFields("id")
                .isEqualTo(savedDepartment);
        assertThat(actual.getId()).isEqualTo(savedDepartment.getId());
    }

    @Test
    void testUpdate_shouldThrowEmployeeNotFoundException() {
        Long managerId = DEPARTMENT_DTO_WRONG_IDS.getManagerId();

        given(employeeRepository.findById(managerId))
                .willReturn(Optional.empty());

        assertThatThrownBy(() -> departmentFacade.update(DEPARTMENT_DTO_WRONG_IDS))
                .isInstanceOf(EmployeeNotFoundException.class)
                .hasMessageContaining("Employee with id " + managerId + " was not found");
        verify(departmentService, never()).save(any());
    }

    @Test
    void testUpdate_shouldThrowLocationNotFoundException() {
        Integer locationId = DEPARTMENT_DTO_WRONG_IDS.getLocationId();

        given(employeeRepository.findById(any()))
                .willReturn(Optional.of(ANY_EMPLOYEE));
        given(locationRepository.findById(locationId))
                .willReturn(Optional.empty());

        assertThatThrownBy(() -> departmentFacade.update(DEPARTMENT_DTO_WRONG_IDS))
                .isInstanceOf(LocationNotFoundException.class)
                .hasMessageContaining("Location with id " + locationId + " was not found");
        verify(departmentService, never()).save(any());
    }

    @Test
    void testUpdate() {
        ArgumentCaptor<Department> argumentCaptor = ArgumentCaptor.forClass(Department.class);

        given(employeeRepository.findById(any()))
                .willReturn(Optional.of(ANY_EMPLOYEE));
        given(locationRepository.findById(any()))
                .willReturn(Optional.of(ANY_LOCATION));

        Department savedDepartment = getDepartmentFromDtoVerified();
        given(departmentService.save(argumentCaptor.capture()))
                .willReturn(savedDepartment);

        DepartmentDTO actual = departmentFacade.update(DEPARTMENT_DTO_VERIFIED);

        verify(departmentService).save(argumentCaptor.getValue());
        assertThat(argumentCaptor.getValue()).isEqualTo(savedDepartment);
        assertThat(actual.getId()).isEqualTo(DEPARTMENT_DTO_VERIFIED.getId());
    }

    @Test
    void testDeleteById_shouldThrowEmployeeNotFoundException() {
        doThrow(DepartmentNotFoundException.class)
                .when(departmentService)
                .deleteById(anyLong());

        assertThatThrownBy(() -> departmentFacade.deleteById(1L))
                .isInstanceOf(DepartmentNotFoundException.class);
    }

    @Test
    void testDeleteById() {
        departmentFacade.deleteById(1L);

        verify(departmentService).deleteById(1L);
    }
}