package com.endava.internship.swaggerdemo.facade;

import com.endava.internship.swaggerdemo.exceptions.DepartmentNotFoundException;
import com.endava.internship.swaggerdemo.exceptions.EmployeeNotFoundException;
import com.endava.internship.swaggerdemo.exceptions.JobNotFoundException;
import com.endava.internship.swaggerdemo.model.Employee;
import com.endava.internship.swaggerdemo.model.dto.EmployeeDTO;
import com.endava.internship.swaggerdemo.model.dto.Identifiable;
import com.endava.internship.swaggerdemo.repository.DepartmentRepository;
import com.endava.internship.swaggerdemo.repository.EmployeeRepository;
import com.endava.internship.swaggerdemo.repository.JobRepository;
import com.endava.internship.swaggerdemo.service.EmployeeService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;
import java.util.Optional;

import static com.endava.internship.swaggerdemo.facade.TestMapperUtils.ANY_DEPARTMENT;
import static com.endava.internship.swaggerdemo.facade.TestMapperUtils.ANY_EMPLOYEE;
import static com.endava.internship.swaggerdemo.facade.TestMapperUtils.ANY_JOB;
import static com.endava.internship.swaggerdemo.facade.TestMapperUtils.EMPLOYEE_DTO_VERIFIED;
import static com.endava.internship.swaggerdemo.facade.TestMapperUtils.EMPLOYEE_DTO_WRONG_IDS;
import static com.endava.internship.swaggerdemo.facade.TestMapperUtils.getEmployeeFromDtoVerified;
import static com.endava.internship.swaggerdemo.util.TestUtils.EMPLOYEES_DEFAULT;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class EmployeeFacadeTest {

    @Mock
    EmployeeService employeeService;

    @Mock
    EmployeeRepository employeeRepository;

    @Mock
    DepartmentRepository departmentRepository;

    @Mock
    JobRepository jobRepository;

    @InjectMocks
    EmployeeFacade employeeFacade;

    @ParameterizedTest
    @MethodSource("com.endava.internship.swaggerdemo.facade.TestMapperUtils#employeeMapperFindAllArgs")
    void testFindAll(boolean expand, List<Identifiable<Long>> expectedList) {
        given(employeeService.findAll())
                .willReturn(EMPLOYEES_DEFAULT);

        List<Identifiable<Long>> actual = employeeFacade.findAll(expand);

        verify(employeeService).findAll();
        assertThat(actual).isEqualTo(expectedList);
    }

    @ParameterizedTest
    @MethodSource("com.endava.internship.swaggerdemo.facade.TestMapperUtils#employeeMapperFindArgs")
    void testFindById(boolean expand, Class<? extends Identifiable<Long>> clazz) {
        given(employeeService.findById(1L))
                .willReturn(Optional.of(new Employee()));

        Identifiable<Long> actual = employeeFacade.findById(1L, expand);

        verify(employeeService).findById(1L);
        assertThat(actual.getClass()).isEqualTo(clazz);
    }

    @Test
    void testFindById_shouldThrowEmployeeNotFoundException() {
        given(employeeService.findById(1L))
                .willReturn(Optional.empty());

        assertThatThrownBy(() -> employeeFacade.findById(1L, true))
                .isInstanceOf(EmployeeNotFoundException.class)
                .hasMessageContaining("Employee with id 1 was not found");
    }

    @Test
    void testSave_shouldThrowDepartmentNotFoundException() {
        Long departmentId = EMPLOYEE_DTO_WRONG_IDS.getDepartmentId();

        given(departmentRepository.findById(departmentId))
                .willReturn(Optional.empty());

        assertThatThrownBy(() -> employeeFacade.save(EMPLOYEE_DTO_WRONG_IDS))
                .isInstanceOf(DepartmentNotFoundException.class)
                .hasMessageContaining("Department with id " + departmentId + " was not found");
        verify(employeeService, never()).save(any());
    }

    @Test
    void testSave_shouldThrowEmployeeNotFoundException() {
        Long managerId = EMPLOYEE_DTO_WRONG_IDS.getManagerId();

        given(departmentRepository.findById(any()))
                .willReturn(Optional.of(ANY_DEPARTMENT));
        given(employeeRepository.findById(managerId))
                .willReturn(Optional.empty());

        assertThatThrownBy(() -> employeeFacade.save(EMPLOYEE_DTO_WRONG_IDS))
                .isInstanceOf(EmployeeNotFoundException.class)
                .hasMessageContaining("Employee with id " + managerId + " was not found");
        verify(employeeService, never()).save(any());
    }

    @Test
    void testSave_shouldThrowJobNotFoundException() {
        String jobId = EMPLOYEE_DTO_WRONG_IDS.getJobId();

        given(departmentRepository.findById(any()))
                .willReturn(Optional.of(ANY_DEPARTMENT));
        given(employeeRepository.findById(any()))
                .willReturn(Optional.of(ANY_EMPLOYEE));
        given(jobRepository.findById(jobId))
                .willReturn(Optional.empty());

        assertThatThrownBy(() -> employeeFacade.save(EMPLOYEE_DTO_WRONG_IDS))
                .isInstanceOf(JobNotFoundException.class)
                .hasMessageContaining("Job with id " + jobId + " was not found");
        verify(employeeService, never()).save(any());
    }

    @Test
    void testSave() {
        ArgumentCaptor<Employee> argumentCaptor = ArgumentCaptor.forClass(Employee.class);

        given(departmentRepository.findById(any()))
                .willReturn(Optional.of(ANY_DEPARTMENT));
        given(employeeRepository.findById(any()))
                .willReturn(Optional.of(ANY_EMPLOYEE));
        given(jobRepository.findById(any()))
                .willReturn(Optional.of(ANY_JOB));

        Employee savedEmployee = getEmployeeFromDtoVerified();
        savedEmployee.setId(1000L);
        given(employeeService.save(argumentCaptor.capture()))
                .willReturn(savedEmployee);

        EmployeeDTO actual = employeeFacade.save(EMPLOYEE_DTO_VERIFIED);

        verify(employeeService).save(argumentCaptor.getValue());
        assertThat(argumentCaptor.getValue())
                .usingRecursiveComparison()
                .ignoringFields("id")
                .isEqualTo(savedEmployee);
        assertThat(actual.getId()).isEqualTo(savedEmployee.getId());

    }

    @Test
    void testUpdate_shouldThrowDepartmentNotFoundException() {
        Long departmentId = EMPLOYEE_DTO_WRONG_IDS.getDepartmentId();

        given(departmentRepository.findById(departmentId))
                .willReturn(Optional.empty());

        assertThatThrownBy(() -> employeeFacade.update(EMPLOYEE_DTO_WRONG_IDS))
                .isInstanceOf(DepartmentNotFoundException.class)
                .hasMessageContaining("Department with id " + departmentId + " was not found");
        verify(employeeService, never()).save(any());
    }

    @Test
    void testUpdate_shouldThrowEmployeeNotFoundException() {
        Long managerId = EMPLOYEE_DTO_WRONG_IDS.getManagerId();

        given(departmentRepository.findById(any()))
                .willReturn(Optional.of(ANY_DEPARTMENT));
        given(employeeRepository.findById(managerId))
                .willReturn(Optional.empty());

        assertThatThrownBy(() -> employeeFacade.update(EMPLOYEE_DTO_WRONG_IDS))
                .isInstanceOf(EmployeeNotFoundException.class)
                .hasMessageContaining("Employee with id " + managerId + " was not found");
        verify(employeeService, never()).save(any());
    }

    @Test
    void testUpdate_shouldThrowJobNotFoundException() {
        String jobId = EMPLOYEE_DTO_WRONG_IDS.getJobId();

        given(departmentRepository.findById(any()))
                .willReturn(Optional.of(ANY_DEPARTMENT));
        given(employeeRepository.findById(any()))
                .willReturn(Optional.of(ANY_EMPLOYEE));
        given(jobRepository.findById(jobId))
                .willReturn(Optional.empty());

        assertThatThrownBy(() -> employeeFacade.update(EMPLOYEE_DTO_WRONG_IDS))
                .isInstanceOf(JobNotFoundException.class)
                .hasMessageContaining("Job with id " + jobId + " was not found");
        verify(employeeService, never()).save(any());
    }

    @Test
    void testUpdate() {
        ArgumentCaptor<Employee> argumentCaptor = ArgumentCaptor.forClass(Employee.class);

        given(departmentRepository.findById(any()))
                .willReturn(Optional.of(ANY_DEPARTMENT));
        given(employeeRepository.findById(any()))
                .willReturn(Optional.of(ANY_EMPLOYEE));
        given(jobRepository.findById(any()))
                .willReturn(Optional.of(ANY_JOB));

        Employee savedEmployee = getEmployeeFromDtoVerified();
        given(employeeService.save(argumentCaptor.capture()))
                .willReturn(savedEmployee);

        EmployeeDTO actual = employeeFacade.update(EMPLOYEE_DTO_VERIFIED);

        verify(employeeService).save(argumentCaptor.getValue());
        assertThat(argumentCaptor.getValue()).isEqualTo(savedEmployee);
        assertThat(actual.getId()).isEqualTo(EMPLOYEE_DTO_VERIFIED.getId());

    }

    @Test
    void testDeleteById_shouldThrowEmployeeNotFoundException() {
        doThrow(EmployeeNotFoundException.class)
                .when(employeeService)
                .deleteById(anyLong());

        assertThatThrownBy(() -> employeeFacade.deleteById(1L))
                .isInstanceOf(EmployeeNotFoundException.class);
    }

    @Test
    void testDeleteById() {
        employeeFacade.deleteById(1L);

        verify(employeeService).deleteById(1L);
    }
}