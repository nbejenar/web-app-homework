package com.endava.internship.swaggerdemo.facade;

import com.endava.internship.swaggerdemo.model.Department;
import com.endava.internship.swaggerdemo.model.Employee;
import com.endava.internship.swaggerdemo.model.Job;
import com.endava.internship.swaggerdemo.model.Location;
import com.endava.internship.swaggerdemo.model.dto.DepartmentDTO;
import com.endava.internship.swaggerdemo.model.dto.DepartmentOnlyName;
import com.endava.internship.swaggerdemo.model.dto.EmployeeDTO;
import com.endava.internship.swaggerdemo.model.dto.EmployeeOnlyNameDepartment;
import org.junit.jupiter.params.provider.Arguments;

import java.util.stream.Stream;

import static com.endava.internship.swaggerdemo.util.TestUtils.DEPARTMENTS_DEFAULT_TO_DTO;
import static com.endava.internship.swaggerdemo.util.TestUtils.DEPARTMENTS_DEFAULT_TO_NAME_ONLY;
import static com.endava.internship.swaggerdemo.util.TestUtils.EMPLOYEES_DEFAULT_TO_DTO;
import static com.endava.internship.swaggerdemo.util.TestUtils.EMPLOYEES_DEFAULT_TO_ONLY_NAME_DEPARTMENT;

public class TestMapperUtils {


    public static final EmployeeDTO EMPLOYEE_DTO_WRONG_IDS = EmployeeDTO.builder()
            .departmentId(-10L)
            .managerId(-10L)
            .jobId("WRONG_JOB_ID")
            .build();

    public static final EmployeeDTO EMPLOYEE_DTO_VERIFIED = EmployeeDTO.builder()
            .id(10L)
            .departmentId(90L)
            .managerId(100L)
            .jobId("IT_SPEC")
            .build();

    public static final Job ANY_JOB = new Job();
    public static final Employee ANY_EMPLOYEE = new Employee();
    public static final Department ANY_DEPARTMENT = new Department();
    public static final Location ANY_LOCATION = new Location();

    public static final DepartmentDTO DEPARTMENT_DTO_WRONG_IDS = DepartmentDTO.builder()
            .managerId(-10L)
            .locationId(-10)
            .build();

    public static final DepartmentDTO DEPARTMENT_DTO_VERIFIED = DepartmentDTO.builder()
            .id(10L)
            .managerId(100L)
            .locationId(1700)
            .build();

    public static final Employee getEmployeeFromDtoVerified() {
        Employee e = EMPLOYEE_DTO_VERIFIED.toEntity();
        e.setJob(ANY_JOB);
        e.setManager(ANY_EMPLOYEE);
        e.setDepartment(ANY_DEPARTMENT);

        return e;
    }

    public static final Department getDepartmentFromDtoVerified() {
        Department department = DEPARTMENT_DTO_VERIFIED.toEntity();
        department.setManager(ANY_EMPLOYEE);
        department.setLocation(ANY_LOCATION);

        return department;
    }

    public static Stream<Arguments> employeeMapperFindArgs() {
        return Stream.of(
                Arguments.of(true, EmployeeDTO.class),
                Arguments.of(false, EmployeeOnlyNameDepartment.class)
        );
    }

    public static Stream<Arguments> employeeMapperFindAllArgs() {
        return Stream.of(
                Arguments.of(true, EMPLOYEES_DEFAULT_TO_DTO),
                Arguments.of(false, EMPLOYEES_DEFAULT_TO_ONLY_NAME_DEPARTMENT)
        );
    }

    public static Stream<Arguments> departmentMapperFindArgs() {
        return Stream.of(
                Arguments.of(true, DepartmentDTO.class),
                Arguments.of(false, DepartmentOnlyName.class)
        );
    }

    public static Stream<Arguments> departmentMapperFindAllArgs() {
        return Stream.of(
                Arguments.of(true, DEPARTMENTS_DEFAULT_TO_DTO),
                Arguments.of(false, DEPARTMENTS_DEFAULT_TO_NAME_ONLY)
        );
    }
}
