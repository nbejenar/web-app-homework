package com.endava.internship.swaggerdemo.repository;

import com.endava.internship.swaggerdemo.model.Employee;
import org.junit.jupiter.params.provider.Arguments;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.stream.Stream;

public class TestRepositoryUtils {

    public static final Employee STEVEN_ID_100 = Employee.builder()
            .id(100L)
            .firstName("Steven")
            .lastName("King")
            .salary(BigDecimal.valueOf(24000))
            .phoneNumber("068242375")
            .hireDate(LocalDate.parse("2003-06-17"))
            .email("SKING@endava.com")
            .commissionPct(null)
            .build();

    public static final Employee EMPLOYEE_TO_ADD = Employee.builder()
            .id(null)
            .firstName("Jora")
            .lastName("VASYOK")
            .salary(BigDecimal.valueOf(24000))
            .phoneNumber("068242365")
            .hireDate(LocalDate.parse("2003-06-17"))
            .email("JVASYOK@endava.com")
            .commissionPct(null)
            .build();

    public static Stream<Arguments> findByIdRequests() {
        return Stream.of(
          Arguments.of(-10L, null),
          Arguments.of(9999L, null),
          Arguments.of(100L, STEVEN_ID_100)
        );
    }
}
