package com.endava.internship.swaggerdemo.repository;

import com.endava.internship.swaggerdemo.model.Employee;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.ClassPathResource;
import org.springframework.jdbc.datasource.init.ScriptUtils;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;
import org.springframework.transaction.annotation.Transactional;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import static com.endava.internship.swaggerdemo.repository.TestRepositoryUtils.EMPLOYEE_TO_ADD;
import static com.endava.internship.swaggerdemo.repository.TestRepositoryUtils.STEVEN_ID_100;
import static com.endava.internship.swaggerdemo.util.TestUtils.GET_ALL_COUNT;
import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
@ActiveProfiles({"test", "jdbc"})
@TestPropertySource(locations = "classpath:application-test.properties")
@Transactional
@Slf4j
class EmployeeRepositoryTest {

    @Autowired
    EmployeeRepository employeeRepository;

    @BeforeAll
    static void setup(@Autowired DataSource dataSource) throws SQLException {
        try (Connection conn = dataSource.getConnection()) {
            conn.setAutoCommit(false);
            ScriptUtils.executeSqlScript(conn, new ClassPathResource("test-dump.sql"));
            conn.commit();
            log.info("||||||||||||||||||| populated h2 db with test data ||||||||||||||||||||");
        } catch (SQLException e) {
            log.error(e.getMessage(), e);
            throw e;
        }
    }

    @Test
    void testFindAll() {
        List<Employee> list = employeeRepository.findAll();

        assertThat(list.size()).isEqualTo(GET_ALL_COUNT);
    }

    @ParameterizedTest
    @MethodSource("com.endava.internship.swaggerdemo.repository.TestRepositoryUtils#findByIdRequests")
    void testFindById(Long id, Employee expected) {
        Employee employee = employeeRepository.findById(id).orElse(null);

        assertThat(employee).usingRecursiveComparison()
                .ignoringFields("manager", "department", "job", "jobHistoryList")
                .isEqualTo(expected);
    }

    @Test
    void testSave_Add() {
        Employee employee = employeeRepository.save(EMPLOYEE_TO_ADD);

        assertThat(employee.getId()).isNotNull();
        assertThat(employee).usingRecursiveComparison()
                .ignoringFields("id")
                .isEqualTo(EMPLOYEE_TO_ADD);
    }

    @Test
    void testSave_Update() {
        Employee employeeToUpdate = STEVEN_ID_100.toBuilder().build();
        String firstName = "Stevenson4ik";
        employeeToUpdate.setFirstName(firstName);

        Employee updated = employeeRepository.save(employeeToUpdate);

        assertThat(updated.getFirstName()).isEqualTo(firstName);
        assertThat(updated).usingRecursiveComparison()
                .ignoringFields("firstName")
                .isEqualTo(employeeToUpdate);
    }

    @Test
    void testDeleteById() {
        Long id = STEVEN_ID_100.getId();
        employeeRepository.deleteById(id);

        assertThat(employeeRepository.findById(id)).isEmpty();
    }
}
