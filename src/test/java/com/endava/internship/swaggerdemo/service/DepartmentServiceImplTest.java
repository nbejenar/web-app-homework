package com.endava.internship.swaggerdemo.service;

import com.endava.internship.swaggerdemo.exceptions.DepartmentNotFoundException;
import com.endava.internship.swaggerdemo.model.Department;
import com.endava.internship.swaggerdemo.repository.DepartmentRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;
import java.util.Optional;

import static com.endava.internship.swaggerdemo.util.TestUtils.DEPARTMENTS_DEFAULT;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class DepartmentServiceImplTest {

    @Mock
    DepartmentRepository departmentRepository;

    @InjectMocks
    DepartmentServiceImpl departmentService;

    @Test
    void testFindAll() {
        given(departmentRepository.findAll())
                .willReturn(DEPARTMENTS_DEFAULT);

        List<Department> list = departmentService.findAll();

        verify(departmentRepository).findAll();
        assertThat(list).isEqualTo(DEPARTMENTS_DEFAULT);
    }

    @ParameterizedTest
    @MethodSource("com.endava.internship.swaggerdemo.service.TestServiceUtils#departmentFindByIdArgs")
    void testFindById_positive(Optional<Department> department, Long id) {
        given(departmentRepository.findById(id))
                .willReturn(department);

        Optional<Department> actual = departmentService.findById(id);

        verify(departmentRepository).findById(id);
        assertThat(actual).isEqualTo(department);
    }

    @ParameterizedTest
    @MethodSource("com.endava.internship.swaggerdemo.service.TestServiceUtils#departmentSaveInputAndOutput")
    void testSave(Department input, Department output) {
        given(departmentRepository.save(input))
                .willReturn(output);

        Department actual = departmentService.save(input);

        verify(departmentRepository).save(input);
        assertThat(actual).isEqualTo(output);
    }

    @Test
    void testDeleteById_shouldThrowDepartmentNotFoundException() {
        given(departmentRepository.findById(anyLong()))
                .willReturn(Optional.empty());

        assertThatThrownBy(() -> {
            departmentService.deleteById(1L);
        }).isInstanceOf(DepartmentNotFoundException.class);
    }

    @Test
    void testDeleteById() {
        ArgumentCaptor<Long> captor = ArgumentCaptor.forClass(Long.class);
        given(departmentRepository.findById(anyLong()))
                .willReturn(Optional.of(new Department()));

        departmentService.deleteById(1L);

        verify(departmentRepository).findById(captor.capture());
        verify(departmentRepository).deleteById(captor.getValue());
    }
}