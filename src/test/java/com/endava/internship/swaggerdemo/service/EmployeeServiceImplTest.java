package com.endava.internship.swaggerdemo.service;

import com.endava.internship.swaggerdemo.exceptions.EmployeeNotFoundException;
import com.endava.internship.swaggerdemo.model.Employee;
import com.endava.internship.swaggerdemo.repository.EmployeeRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;
import java.util.Optional;

import static com.endava.internship.swaggerdemo.util.TestUtils.EMPLOYEES_DEFAULT;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class EmployeeServiceImplTest {

    @Mock
    EmployeeRepository employeeRepository;

    @InjectMocks
    EmployeeServiceImpl employeeService;

    @Test
    void testFindAll() {
        given(employeeRepository.findAll())
                .willReturn(EMPLOYEES_DEFAULT);

        List<Employee> list = employeeService.findAll();

        verify(employeeRepository).findAll();
        assertThat(list).isEqualTo(EMPLOYEES_DEFAULT);
    }

    @ParameterizedTest
    @MethodSource("com.endava.internship.swaggerdemo.service.TestServiceUtils#employeeFindByIdArgs")
    void testFindById_positive(Optional<Employee> employee, Long id) {
        given(employeeRepository.findById(id))
                .willReturn(employee);

        Optional<Employee> actual = employeeService.findById(id);

        verify(employeeRepository).findById(id);
        assertThat(actual).isEqualTo(employee);
    }

    @ParameterizedTest
    @MethodSource("com.endava.internship.swaggerdemo.service.TestServiceUtils#employeeSaveInputAndOutput")
    void testSave(Employee input, Employee output) {
        given(employeeRepository.save(input))
                .willReturn(output);

        Employee actual = employeeService.save(input);

        verify(employeeRepository).save(input);
        assertThat(actual).isEqualTo(output);
    }

    @Test
    void testDeleteById_shouldThrowEmployeeNotFoundException() {
        given(employeeRepository.findById(anyLong()))
                .willReturn(Optional.empty());

        assertThatThrownBy(() -> {
            employeeService.deleteById(1L);
        }).isInstanceOf(EmployeeNotFoundException.class);
    }

    @Test
    void testDeleteById() {
        ArgumentCaptor<Long> captor = ArgumentCaptor.forClass(Long.class);
        given(employeeRepository.findById(anyLong()))
                .willReturn(Optional.of(new Employee()));

        employeeService.deleteById(1L);

        verify(employeeRepository).findById(captor.capture());
        verify(employeeRepository).deleteById(captor.getValue());
    }
}