package com.endava.internship.swaggerdemo.service;

import com.endava.internship.swaggerdemo.model.Department;
import com.endava.internship.swaggerdemo.model.Employee;
import org.junit.jupiter.params.provider.Arguments;

import java.util.Optional;
import java.util.stream.Stream;

public class TestServiceUtils {

    public static Stream<Arguments> employeeFindByIdArgs() {
        return Stream.of(
                Arguments.of(Optional.of(new Employee()), 1L),
                Arguments.of(Optional.empty(), 2L)
        );
    }

    public static Stream<Arguments> employeeSaveInputAndOutput() {
        Employee a1 = new Employee();
        Employee a2 = new Employee();
        a2.setId(1L);

        return Stream.of(
                Arguments.of(a1, a2)
        );
    }

    public static Stream<Arguments> departmentFindByIdArgs() {
        return Stream.of(
                Arguments.of(Optional.of(new Department()), 1L),
                Arguments.of(Optional.empty(), 2L)
        );
    }

    public static Stream<Arguments> departmentSaveInputAndOutput() {
        Department a1 = new Department();
        Department a2 = new Department();
        a2.setId(1L);

        return Stream.of(
                Arguments.of(a1, a2)
        );
    }
}
