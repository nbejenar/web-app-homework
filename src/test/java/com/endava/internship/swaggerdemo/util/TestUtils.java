package com.endava.internship.swaggerdemo.util;

import com.endava.internship.swaggerdemo.exceptions.ErrorResponse;
import com.endava.internship.swaggerdemo.model.Department;
import com.endava.internship.swaggerdemo.model.Employee;
import com.endava.internship.swaggerdemo.model.Location;
import com.endava.internship.swaggerdemo.model.dto.DepartmentDTO;
import com.endava.internship.swaggerdemo.model.dto.DepartmentOnlyName;
import com.endava.internship.swaggerdemo.model.dto.EmployeeDTO;
import com.endava.internship.swaggerdemo.model.dto.EmployeeOnlyNameDepartment;
import com.endava.internship.swaggerdemo.model.dto.Identifiable;
import org.junit.jupiter.params.provider.Arguments;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;
import java.util.Locale;
import java.util.stream.Stream;

import static com.endava.internship.swaggerdemo.exceptions.ErrorMessage.EMPLOYEE_NOT_FOUND_EXCEPTION;
import static com.endava.internship.swaggerdemo.exceptions.ErrorMessage.NUMBER_FORMAT_EXCEPTION;

@Component
public class TestUtils {

    private static MessageSource messageSource;
    public static final String ENDPOINT = "/api/v1.0/";
    public static final Integer GET_ALL_COUNT = 107;
    public static final Long NON_EXISTENT_EMPLOYEE_ID = -10L;
    public static final Long EXISTENT_EMPLOYEE_ID = 100L;
    public static final List<Employee> EMPLOYEES_DEFAULT = List.of(
            Employee.builder().firstName("emp1").phoneNumber("069222666").build(),
            Employee.builder().firstName("emp2").phoneNumber("069222666").build(),
            Employee.builder().firstName("emp3").phoneNumber("069222666").build()
    );

    public static final List<EmployeeDTO> EMPLOYEES_DEFAULT_TO_DTO = List.of(
            EmployeeDTO.builder().firstName("emp1").phoneNumber("069222666").build(),
            EmployeeDTO.builder().firstName("emp2").phoneNumber("069222666").build(),
            EmployeeDTO.builder().firstName("emp3").phoneNumber("069222666").build()
    );

    public static final List<EmployeeOnlyNameDepartment> EMPLOYEES_DEFAULT_TO_ONLY_NAME_DEPARTMENT = List.of(
            EmployeeOnlyNameDepartment.builder().firstName("emp1").build(),
            EmployeeOnlyNameDepartment.builder().firstName("emp2").build(),
            EmployeeOnlyNameDepartment.builder().firstName("emp3").build()
    );

    private static final Location DUMMY_LOCATION = Location.builder().id(1).city("CityName").build();

    public static final List<Department> DEPARTMENTS_DEFAULT = List.of(
            Department.builder().departmentName("dep1").location(DUMMY_LOCATION).build(),
            Department.builder().departmentName("dep2").location(DUMMY_LOCATION).build(),
            Department.builder().departmentName("dep3").location(DUMMY_LOCATION).build()
    );

    public static final List<DepartmentDTO> DEPARTMENTS_DEFAULT_TO_DTO = List.of(
            DepartmentDTO.builder().departmentName("dep1").locationId(DUMMY_LOCATION.getId()).build(),
            DepartmentDTO.builder().departmentName("dep2").locationId(DUMMY_LOCATION.getId()).build(),
            DepartmentDTO.builder().departmentName("dep3").locationId(DUMMY_LOCATION.getId()).build()
    );

    public static final List<DepartmentOnlyName> DEPARTMENTS_DEFAULT_TO_NAME_ONLY = List.of(
            DepartmentOnlyName.builder().name("dep1").build(),
            DepartmentOnlyName.builder().name("dep2").build(),
            DepartmentOnlyName.builder().name("dep3").build()
    );

    public static final Identifiable<Long> EXPECTED_FIND_BY_ID_100 = EmployeeOnlyNameDepartment.builder()
            .id(100L)
            .departmentId(90L)
            .firstName("Steven")
            .lastName("King").build();

    public static final Identifiable<Long> EXPECTED_FIND_BY_ID_100_EXPANDED = EmployeeDTO.builder()
            .id(100L)
            .departmentId(90L)
            .firstName("Steven")
            .lastName("King")
            .salary(BigDecimal.valueOf(24000))
            .phoneNumber("068242375")
            .hireDate(LocalDate.parse("2003-06-17"))
            .email("SKING@endava.com")
            .commissionPct(null)
            .managerId(null)
            .jobId("AD_PRES")
            .build();

    public static final Identifiable<Long> EXPECTED_FIND_BY_ID_100_TO_UPDATE = EmployeeDTO.builder()
            .id(100L)
            .departmentId(90L)
            .firstName("Stevenson")
            .lastName("King")
            .salary(BigDecimal.valueOf(24000))
            .phoneNumber("068242375")
            .hireDate(LocalDate.parse("2003-06-17"))
            .email("SKING@endava.com")
            .commissionPct(null)
            .managerId(null)
            .jobId("AD_PRES")
            .build();


    @Qualifier("messageSource")
    @Autowired
    MessageSource tmpSource;

    @PostConstruct
    public void init() {
        System.out.println(tmpSource);
        messageSource = tmpSource;
    }

    public static Stream<Arguments> badFindUserByIdRequests() {
        return Stream.of(
                Arguments.of(9999L, new ErrorResponse(404, getMessage(EMPLOYEE_NOT_FOUND_EXCEPTION, "ru", 9999L)), "ru"),
                Arguments.of(9999L, new ErrorResponse(404, getMessage(EMPLOYEE_NOT_FOUND_EXCEPTION, "en", 9999L)), "en"),
                Arguments.of("abc", new ErrorResponse(400, getMessage(NUMBER_FORMAT_EXCEPTION, "ru")), "ru"),
                Arguments.of("abc", new ErrorResponse(400, getMessage(NUMBER_FORMAT_EXCEPTION, "en")), "en")
        );
    }

    private static String getMessage(String message, String lang, Object... objects) {
        return messageSource.getMessage(message, objects, new Locale(lang));
    }

}
