﻿# Web Application Homework


## Setup Steps  
  
1. you should have Docker Desktop on your machine.  
2. `docker pull aleanca/oracledb-12.2.0.1-ee:12.2.0.1-ee-01`  
3. `cd <this project directory>/docker`  
4. `docker compose up -d`  
- username - swagger
- password - swagger
- jdbc url - `jdbc:oracle:thin:@//localhost:1521/PDB`  

  
## Happy Reviewing!


## The Objective

This task is very important in order to summarize all the knowledge gained during past 4 weeks. That means you will apply almost all the theoretical knowledge in practice in one place. Mainly you have to implement a simple web application with exposed endpoints to perform CRUD ops for two resources. Keep in mind that you should follow layered architecture approach.

Layered architecture:

![](https://lh6.googleusercontent.com/UwDf1S7MapyAFiPOP-EUyd2GvzCg534Cb0JorhB3Uktus5O9g30PTJssSSdDTo-VXt_vvM_BrbH8qv8J9xvgAhjfGXYz-CL1MfHFyLHtij0Eux9JV_geccjqcq6mY3mLjuS-0u0L)

## Initial Source Code

No initial sources for this time. Create new repositories within your bitbucket/github accounts. Include in your projects the docker compose file from your previous task.

## The Assignment

You should already be familiar with HR schema, so let's use it. You have to map tables to java entities, implement the DAO layer and expose endpoints for CRUD ops for employees and departments.
![](https://sun9-39.userapi.com/impg/3XwYwp6uQ6FlLJkgUo6oPswAtiZKnyiYTPnu2g/Ez30LERUUss.jpg?size=1128x679&quality=96&sign=2ad1e6cfb168897d57400d5a95856c95&type=album)
Base url: localhost:8080/api/

GET /employees - list all employees

GET /employees/{id} - get employee by id

POST /employees - create new employee

PUT /employees - update an existing employee

DELETE /employees - remove an employee

For employees resource consider the "expand" query param (simple boolean flag). If true, response should contain the entire representation of employee (all the raws from table). Otherwise return only first name, last name and department id.

GET /departments - list all departments

GET /departments/{id} - get department by id

POST /departments - create new department

PUT /departments - update an existing department

DELETE /departments - remove department

For departments resource consider the "expand" query param (simple boolean flag). If true, response should contain the entire representation of department (all the raws from table). Otherwise return only department name.

Add the input validation for employee as following (any constraint violation should lead to response status code 400 - BAD_REQUEST with descriptive error message):

-   First name and Last name - not to be null, empty or blank
    
-   Email - to match email format
    
-   Phone number - to start with 0 and and contain exactly 9 digits
    
-   Salary - min 1.0
    

And for department:

-   Name - not to be null, empty or blank
    

Additional requirements:

- 85% unit test coverage

- integration tests for employees related flows

- OpenAPI (swagger)
